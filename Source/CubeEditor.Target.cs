using UnrealBuildTool;
using System.Collections.Generic;

public class CubeEditorTarget : TargetRules
{
    public CubeEditorTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Editor;
        DefaultBuildSettings = BuildSettingsVersion.V2;
        IncludeOrderVersion = EngineIncludeOrderVersion.Latest;
        ExtraModuleNames.AddRange(new string[] {"Cube", "CubeEditor"});
    }
}
