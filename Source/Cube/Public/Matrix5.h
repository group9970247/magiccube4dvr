#pragma once

#include "Vector5.h"

class FMatrix5
{
public:
    static const FMatrix5 Identity;


    static FMatrix5 FromColumns(const FVector5& Column0, const FVector5& Column1, const FVector5& Column2, const FVector5& Column3, const FVector5& Column4);


    FMatrix5() = default;
    FMatrix5(const FVector5& Row0, const FVector5& Row1, const FVector5& Row2, const FVector5& Row3, const FVector5& Row4);

    friend FMatrix5 operator*(const FMatrix5& A, const FMatrix5& B);
    const FVector5& operator[](int8 Index) const;
    FVector5& operator[](int8 Index);
    friend FMatrix5 operator*(const FMatrix5& A, float B);
    friend FArchive& operator<<(FArchive& Ar, FMatrix5& Matrix);
    friend FVector5 operator*(const FMatrix5& A, const FVector5& B);

    FVector5 GetColumn(int8 Index) const;
    FVector3f GetColumn3(int8 Index) const;
    void SetColumn(int8 ColumnIndex, const FVector5& Column);

private:
    FVector5 Rows[5];
};
