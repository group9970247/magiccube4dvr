#pragma once

#include "CoreMinimal.h"

class FVector5
{
public:
    FVector5();
    FVector5(float Data0, float Data1, float Data2, float Data3, float Data4);

    float operator[](int8 Index) const;
    float& operator[](int8 Index);
    FVector5 operator-() const;
    friend FArchive& operator<<(FArchive& Ar, FVector5& Vector);
    friend FVector5 operator+(const FVector5& A, const FVector5& B);
    friend FVector5 operator*(const FVector5& Vector, float Scalar);

private:
    float Data[5];
};
