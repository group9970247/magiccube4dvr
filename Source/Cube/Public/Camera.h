#pragma once

#include "Math/MathFwd.h"
#include "Matrix5.h"

enum class ECameraRotationPlane : int32
{
    Plane01 = 0,
    Plane12 = 1,
    Plane23 = 2
};

enum class ECameraRotationState : uint8
{
    None = 0,
    Active = 1,
    End = 2
};

class CUBE_API FCamera
{
public:
    static void Initialize();
#if WITH_EDITOR
    static void SetAngles(const FVector3f& AnglesIn);
#endif
    static void Tick(float DeltaSeconds);

    static const FVector3f& GetAngles();
    static const FMatrix44f& GetRotation012();
    static const FMatrix5& GetProjection();

    static ECameraRotationState GetCameraRotation();
    static bool RotationIsActive();

    static void Rotate(ECameraRotationPlane CameraRotationPlane, float Value);
    static void Reset();

private:
    static FVector3f Angles;
    static FMatrix44f Rotation012;
    static FMatrix5 Projection;

    static FVector3f Frequencies;
    static ECameraRotationState CameraRotationState;
    static float EndTime;


    static void UpdateProjection();
    static void CameraRotated();
};
