using UnrealBuildTool;

public class Cube : ModuleRules
{
    public Cube(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        PrivateDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "EnhancedInput",
            "HeadMountedDisplay",
            "UMG",
        });
    }
}
