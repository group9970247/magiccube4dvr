#pragma once

#include "Materials/MaterialExpression.h"
#include "Projection.generated.h"

UENUM()
enum EProjectionSpace
{
    PROJECTION_Local UMETA(DisplayName="Local Space"),
    PROJECTION_Instance UMETA(DisplayName = "Instance Space"),

    PROJECTION_MAX
};

UCLASS()
class UProjection : public UMaterialExpression
{
    GENERATED_BODY()

public:
    UPROPERTY()
    FExpressionInput PositionRow0;

    UPROPERTY()
    FExpressionInput PositionRow1;

    UPROPERTY()
    FExpressionInput PositionRow2;

    UPROPERTY()
    FExpressionInput NormalTS;

    UPROPERTY()
    FExpressionInput NormalRow0;

    UPROPERTY()
    FExpressionInput NormalRow1;

    UPROPERTY()
    FExpressionInput NormalRow2;

    UPROPERTY(EditAnywhere)
    TEnumAsByte<EProjectionSpace> Space = PROJECTION_Local;


    UProjection();

#if WITH_EDITOR
    virtual int32 Compile(FMaterialCompiler* Compiler, int32 OutputIndex) override;
    virtual void GetCaption(TArray<FString>& OutCaptions) const override;
    virtual uint32 GetOutputType(int32 OutputIndex) override;

private:
    FMaterialCompiler* CompilerPrivate;


    int32 Transform(FExpressionInput& Row0, FExpressionInput& Row1, FExpressionInput& Row2, int32 Vector) const;
#endif
};
