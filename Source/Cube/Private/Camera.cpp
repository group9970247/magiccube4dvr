#include "Camera.h"
#include "Utils.h"
#include "CubeActor.h"
#include "CubePawn.h"


FVector3f FCamera::Angles;
FMatrix44f FCamera::Rotation012;
FMatrix5 FCamera::Projection;

FVector3f FCamera::Frequencies;
ECameraRotationState FCamera::CameraRotationState;
float FCamera::EndTime;


void FCamera::Initialize()
{
    Angles = {-0.8f, 2.2f, 1.f};

    FMatrix44f& A = Rotation012;
                                                       A.M[3][0] = 0.f;
                                                       A.M[3][1] = 0.f;
                                                       A.M[3][2] = 0.f;
    A.M[0][3] = 0.f; A.M[1][3] = 0.f; A.M[2][3] = 0.f; A.M[3][3] = 1.f;

    FMatrix5& P = Projection;
                                                 P[0][3] = 0.f; P[0][4] = 0.f;
                                                 P[1][3] = 0.f; P[1][4] = 0.f;
                                                                P[2][4] = 0.f;
                                                                P[3][4] = -200.f;
    P[4][0] = 0.f; P[4][1] = 0.f; P[4][2] = 0.f; P[4][3] = 0.f; P[4][4] = 1.f;

    UpdateProjection();
    Frequencies = FVector3f::ZeroVector;
}

#if WITH_EDITOR
void FCamera::SetAngles(const FVector3f& AnglesIn)
{
    Angles = AnglesIn;
    CameraRotated();
}
#endif

void FCamera::Tick(float DeltaSeconds)
{
    if (RotationIsActive())
    {
        bool bRotate01Zero = FMath::IsNearlyZero(Frequencies[(int32)ECameraRotationPlane::Plane01]);
        bool bRotate12Zero = FMath::IsNearlyZero(Frequencies[(int32)ECameraRotationPlane::Plane12]);
        bool bRotate23Zero = FMath::IsNearlyZero(Frequencies[(int32)ECameraRotationPlane::Plane23]);
    
        auto DeltaAngle = [DeltaSeconds](float Frequency)
        {
            float FrequencyScale = 5.f;
            return Frequency * FrequencyScale * DeltaSeconds;
        };
    
        if (!bRotate01Zero || !bRotate12Zero)
        {
            if (FMath::Abs(Frequencies[(int32)ECameraRotationPlane::Plane01]) >=
                FMath::Abs(Frequencies[(int32)ECameraRotationPlane::Plane12]))
                Angles[(int32)ECameraRotationPlane::Plane01] = FMath::Fractional((
                    Angles[(int32)ECameraRotationPlane::Plane01] +
                    DeltaAngle(Frequencies[(int32)ECameraRotationPlane::Plane01])) / TWO_PI) * TWO_PI;
            else
                Angles[(int32)ECameraRotationPlane::Plane12] = FMath::Clamp(
                    Angles[(int32)ECameraRotationPlane::Plane12] -
                    DeltaAngle(Frequencies[(int32)ECameraRotationPlane::Plane12]), 0.f, PI);
        }
    
        if (!bRotate23Zero)
            Angles[(int32)ECameraRotationPlane::Plane23] = FMath::Clamp(
                Angles[(int32)ECameraRotationPlane::Plane23] +
                DeltaAngle(Frequencies[(int32)ECameraRotationPlane::Plane23]), 0.f, PI);
    
        CameraRotated();
    }
    else if (CameraRotationState == ECameraRotationState::End)
    {
        EndTime -= DeltaSeconds;
        if (EndTime < 0.f)
            Reset();
    }
}

const FVector3f& FCamera::GetAngles()
{
    return Angles;
}

const FMatrix44f& FCamera::GetRotation012()
{
    return Rotation012;
}

const FMatrix5& FCamera::GetProjection()
{
    return Projection;
}

ECameraRotationState FCamera::GetCameraRotation()
{
    return CameraRotationState;
}

bool FCamera::RotationIsActive()
{
    return CameraRotationState == ECameraRotationState::Active;
}

void FCamera::Rotate(ECameraRotationPlane CameraRotationPlane, float Value)
{
    bool bActive = RotationIsActive();

    if (!bActive && FMath::IsNearlyZero(Value))
        return;

    Frequencies[(int32)CameraRotationPlane] = Value;

    if (bActive)
    {
        if (Frequencies.IsNearlyZero())
            CameraRotationState = ECameraRotationState::End;
    }
    else
        CameraRotationState = ECameraRotationState::Active;
}

void FCamera::Reset()
{
    CameraRotationState = ECameraRotationState::None;
    FUtils::GetPawn()->ResetCameraHand();
    EndTime = 2.f;
}

void FCamera::UpdateProjection()
{
    float SinF, CosF;
    FMath::SinCos(&SinF, &CosF, Angles[(int32)ECameraRotationPlane::Plane01]);

    float SinP, CosP;
    FMath::SinCos(&SinP, &CosP, Angles[(int32)ECameraRotationPlane::Plane12]);

    float SinT, CosT;
    FMath::SinCos(&SinT, &CosT, Angles[(int32)ECameraRotationPlane::Plane23]);

    FMatrix44f& A = Rotation012;
    A.M[0][0] = CosF;         A.M[1][0] = SinF;         A.M[2][0] = 0.f;
    A.M[0][1] = -SinF * CosP; A.M[1][1] = CosF * CosP;  A.M[2][1] = SinP;
    A.M[0][2] = SinF * SinP;  A.M[1][2] = -CosF * SinP; A.M[2][2] = CosP;

    float B11 = CosT;  float B12 = SinT;
    float B21 = -SinT; float B22 = CosT;

    FMatrix5& P = Projection;
    P[0][0] = A.M[0][0];       P[0][1] = A.M[1][0];       P[0][2] = A.M[2][0];
    P[1][0] = A.M[0][1];       P[1][1] = A.M[1][1];       P[1][2] = A.M[2][1];
    P[2][0] = A.M[0][2] * B11; P[2][1] = A.M[1][2] * B11; P[2][2] = A.M[2][2] * B11; P[2][3] = B12;
    P[3][0] = A.M[0][2] * B21; P[3][1] = A.M[1][2] * B21; P[3][2] = A.M[2][2] * B21; P[3][3] = B22;
}

void FCamera::CameraRotated()
{
    UpdateProjection();
    FUtils::GetCubeActor()->UpdateSubcubes();
}
