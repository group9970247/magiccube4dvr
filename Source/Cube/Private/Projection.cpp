#include "Projection.h"
#include "MaterialCompiler.h"

UProjection::UProjection()
{
#if WITH_EDITORONLY_DATA
    bShowOutputNameOnPin = true;
    Outputs.Reset();

    Outputs.Add(FExpressionOutput("NormalTS"));
    Outputs.Add(FExpressionOutput("WorldPositionOffset"));
#endif
}

#if WITH_EDITOR
int32 UProjection::Compile(FMaterialCompiler* Compiler, int32 OutputIndex)
{
    if (!PositionRow0.GetTracedInput().Expression ||
        !PositionRow1.GetTracedInput().Expression ||
        !PositionRow2.GetTracedInput().Expression ||
        !NormalTS.GetTracedInput().Expression ||
        !NormalRow0.GetTracedInput().Expression ||
        !NormalRow1.GetTracedInput().Expression ||
        !NormalRow2.GetTracedInput().Expression)
        return Compiler->Constant3(0.f, 0.f, 0.f);

    CompilerPrivate = Compiler;
    EMaterialCommonBasis Basis = Space == PROJECTION_Local ? MCB_Local : MCB_Instance;

    if (OutputIndex == 0)
    {
        int32 NormalBS = Compiler->TransformVector(MCB_Tangent, Basis, NormalTS.Compile(Compiler));
        int32 NewNormalBS = Transform(NormalRow0, NormalRow1, NormalRow2, NormalBS);
        int32 NewNormalTS = Compiler->TransformVector(Basis, MCB_Tangent, NewNormalBS);
        return Compiler->Normalize(NewNormalTS);
    }
    else
    {
        int32 PositionWS = Compiler->WorldPosition(WPT_Default);
        int32 PositionBS = Compiler->TransformPosition(MCB_World, Basis, PositionWS);
        int32 Position4 = Compiler->AppendVector(PositionBS, Compiler->Constant(1.f));
        int32 NewPositionBS = Transform(PositionRow0, PositionRow1, PositionRow2, Position4);
        int32 NewPositionWS = Compiler->TransformPosition(Basis, MCB_World, NewPositionBS);
        return Compiler->Sub(NewPositionWS, PositionWS);
    }
}

void UProjection::GetCaption(TArray<FString>& OutCaptions) const
{
    OutCaptions.Add("Projection");
}

uint32 UProjection::GetOutputType(int32 OutputIndex)
{
    return MCT_Float3;
}

int32 UProjection::Transform(FExpressionInput& Row0, FExpressionInput& Row1, FExpressionInput& Row2, int32 Vector) const
{
    int32 New0 = CompilerPrivate->Dot(Row0.Compile(CompilerPrivate), Vector);
    int32 New1 = CompilerPrivate->Dot(Row1.Compile(CompilerPrivate), Vector);
    int32 New2 = CompilerPrivate->Dot(Row2.Compile(CompilerPrivate), Vector);

    int32 New01 = CompilerPrivate->AppendVector(New0, New1);
    return CompilerPrivate->AppendVector(New01, New2);
}
#endif
