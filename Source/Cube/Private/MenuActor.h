#pragma once

#include "GameFramework/Actor.h"
#include "MenuActor.generated.h"

class USoundWave;

UCLASS()
class AMenuActor : public AActor
{
    GENERATED_BODY()

public:
    AMenuActor();
    void Initialize();

    class UMenuWidget* GetMenu();
    void SwitchMenu();

    void Select() const;
    void Press() const;

private:
    USoundWave* SelectSound;
    USoundWave* PressSound;


    void PreloadSounds();
};
