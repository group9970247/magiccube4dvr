#pragma once
#include "GameFramework/GameModeBase.h"
#include "CubeGameMode.generated.h"

class UAnimSequence;

UCLASS()
class ACubeGameMode : public AGameModeBase
{
    GENERATED_BODY()

public:
    ACubeGameMode();

    USkeletalMeshComponent* GetSkeletalMesh(EControllerHand Hand) const;

    virtual void Tick(float DeltaSeconds) override;
    virtual void BeginPlay() override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
    UPROPERTY()
    USkeletalMeshComponent* SkeletalMeshLeft;

    UPROPERTY()
    USkeletalMeshComponent* SkeletalMeshRight;


    int8 TickIndex = 0;

    UAnimSequence* LeftGrab;
    UAnimSequence* RightGrab;
};
