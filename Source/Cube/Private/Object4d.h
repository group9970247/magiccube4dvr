#pragma once
#include "Matrix5.h"

class FObject4d
{
public:
    virtual ~FObject4d();

    void Rotate(const FMatrix5& Rotation);
    void Update();
    void SaveTransform();

    const FMatrix5& GetMatrix() const;
    const FMatrix5& GetTransform() const;
    void SetTransform(const FMatrix5& TransformIn);

protected:
    FObject4d();

    virtual bool UpdateInternal() = 0;
    virtual void SetRow4(int8 RowIndex) = 0;
    virtual void SetRow3(int8 RowIndex, float R0, float R1, float R2) = 0;

private:
    FMatrix5 Transform;
    FMatrix5 Matrix;
    FMatrix5 TransformBeforeGrab;
};
