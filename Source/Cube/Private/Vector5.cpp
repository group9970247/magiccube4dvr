#include "Vector5.h"

FVector5::FVector5()
{
    for (int8 Index = 0; Index < 5; Index++)
        Data[Index] = 0.f;
}

FVector5::FVector5(float Data0, float Data1, float Data2, float Data3, float Data4)
{
    Data[0] = Data0;
    Data[1] = Data1;
    Data[2] = Data2;
    Data[3] = Data3;
    Data[4] = Data4;
}

float FVector5::operator[](int8 Index) const
{
    return Data[Index];
}

float& FVector5::operator[](int8 Index)
{
    return Data[Index];
}

FVector5 FVector5::operator-() const
{
    return {-Data[0], -Data[1], -Data[2], -Data[3], -Data[4]};
}

FArchive& operator<<(FArchive& Ar, FVector5& Vector)
{
    for (int8 Index = 0; Index < 5; Index++)
        Ar << Vector.Data[Index];

    return Ar;
}

FVector5 operator+(const FVector5& A, const FVector5& B)
{
    return {A[0] + B[0], A[1] + B[1], A[2] + B[2], A[3] + B[3], A[4] + B[4]};
}

FVector5 operator*(const FVector5& Vector, float Scalar)
{
    return {Vector[0] * Scalar, Vector[1] * Scalar, Vector[2] * Scalar, Vector[3] * Scalar, Vector[4] * Scalar};
}
