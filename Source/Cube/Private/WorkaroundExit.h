#pragma once
#include "HAL/Runnable.h"

class FWorkaroundExit : public FRunnable
{
public:
    static void Execute();


    virtual uint32 Run() override;
};
