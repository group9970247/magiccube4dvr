#pragma once

#include "GameFramework/Actor.h"
#include "Spectator.generated.h"

UCLASS()
class ASpectator : public AActor
{
    GENERATED_BODY()

public:
    ASpectator();

    virtual void Tick(float DeltaSeconds) override;

    void Enable();

private:
    class UTextureRenderTarget2D* RenderTarget;
};
