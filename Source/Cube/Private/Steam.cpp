#include "Steam.h"
#include "Utils.h"
#include "Subcube.h"
#include "MenuWidget.h"


FSteam* FSteam::Instance;
FRunnableThread* FSteam::Thread;
TArray<FSteam::FAchievement> FSteam::Achievements;
TAtomic<bool> FSteam::bStop;
FCriticalSection FSteam::GridStatesCriticalSection;
TDoubleLinkedList<FCubeGrid> FSteam::GridStates;

void FSteam::Initialize()
{
    Instance = new FSteam;
}

void FSteam::Deinitialize()
{
    if (Instance)
    {
        bStop = true;
        delete Thread;
    }

    delete Instance;
}

void FSteam::CommitGridState()
{
    if (FUtils::GetMenuWidget()->GetSolved())
        return;

    FScopeLock Lock(&GridStatesCriticalSection);
    GridStates.AddTail(*FUtils::GetConstGrid());

    while (GridStates.Num() > 1024)
        GridStates.RemoveNode(GridStates.GetHead());
}

uint32 FSteam::Run()
{
    while (!bStop)
    {
        FPlatformProcess::Sleep(1.f);

        for (;;)
        {
            TDoubleLinkedList<FCubeGrid>::TDoubleLinkedListNode* Head;
            {
                FScopeLock Lock(&GridStatesCriticalSection);

                Head = GridStates.GetHead();
                if (Head == nullptr)
                    break;

                GridStates.RemoveNode(Head, false);
            }

            const FCubeGrid& Grid = Head->GetValue();

            for (FAchievement& Achievement : Achievements)
            {
                if (Achievement.bAchieved)
                    continue;

                for (const TArray<FIntVector4>& And : Achievement.Or)
                {
                    bool bAchieved = true;

                    for (const FIntVector4& Cell : And)
                    {
                        if (Grid.At(Cell)->GetInitialCell() == Cell)
                            continue;

                        bAchieved = false;
                        break;
                    }

                    if (!bAchieved)
                        continue;

                    Achievement.bAchieved = true;
                    //you achieved Achievement.Id
                    break;
                }
            }

            delete Head;
        }
    }

    return 0;
}

FSteam::FSteam()
{
    Add3Achievements([this](int8 MinZeroCount)
        {
            AddAchievement(2, MinZeroCount);
        });

    Achievements[0].Id = "CENTERS_OF_ONE_SIDE";
    Achievements[1].Id = "CENTERS_AND_EDGES_OF_ONE_SIDE";
    Achievements[2].Id = "ONE_SIDE";


    Add3Achievements([this](int8 MinZeroCount)
        {
            AddAchievement(3, MinZeroCount);
        });

    Achievements[3].Id = "CENTERS_OF_TWO_LAYERS";
    Achievements[4].Id = "CENTERS_AND_EDGES_OF_TWO_LAYERS";
    Achievements[5].Id = "TWO_LAYERS";


    Add3Achievements([this](int8 MinZeroCount)
        {
            AddOr(3, true, 5, MinZeroCount);
        });

    Achievements[6].Id = "ALL_CENTERS";
    Achievements[7].Id = "ALL_CENTERS_AND_EDGES";
    Achievements[8].Id = "WHOLE_CUBE";


    bStop = false;
    Thread = FRunnableThread::Create(this, TEXT("Steam"));
}

void FSteam::Add3Achievements(TFunction<void(int8 MinZeroCount)> Add)
{
    for (int8 MinZeroCount = 2; MinZeroCount >= 0; MinZeroCount--)
    {
        Achievements.AddDefaulted();
        Add(MinZeroCount);
    }
}

void FSteam::AddAchievement(int8 Height, int8 MinZeroCount)
{
    for (int8 SideAxis = 0; SideAxis < 4; SideAxis++)
    {
        for (int8 Positive = 0; Positive < 2; Positive++)
            AddOr(SideAxis, bool(Positive), Height, MinZeroCount);
    }
}

void FSteam::AddOr(int8 SideAxis, bool bPositive, int8 Height, int8 MinZeroCount)
{
    TArray<FIntVector4>& And = Achievements.Last().Or.AddDefaulted_GetRef();

    FIntVector4 Start = {-2, -2, -2, -2};
    Start[SideAxis] = bPositive ? 3 - Height : -2;

    FIntVector4 End = {3, 3, 3, 3};
    End[SideAxis] = bPositive ? 3 : -2 + Height;

    FCubeGrid::ForEach(Start, End, [MinZeroCount, &And](const FIntVector4& Cell)
        {
            int8 AbsTwoCount = 0;
            int8 ZeroCount = 0;

            for (int32 Index = 0; Index < 4; Index++)
            {
                int8 Value = Cell[Index];
                AbsTwoCount += FMath::Abs(Value) == 2;
                ZeroCount += Value == 0;
            }

            if (AbsTwoCount == 1 && ZeroCount >= MinZeroCount)
                And.Add(Cell);
        });
}
