#include "Matrix5.h"

const FMatrix5 FMatrix5::Identity = {
    {1.f, 0.f, 0.f, 0.f, 0.f},
    {0.f, 1.f, 0.f, 0.f, 0.f},
    {0.f, 0.f, 1.f, 0.f, 0.f},
    {0.f, 0.f, 0.f, 1.f, 0.f},
    {0.f, 0.f, 0.f, 0.f, 1.f}};

FMatrix5 FMatrix5::FromColumns(const FVector5& Column0, const FVector5& Column1, const FVector5& Column2, const FVector5& Column3, const FVector5& Column4)
{
    return {
        {Column0[0], Column1[0], Column2[0], Column3[0], Column4[0]},
        {Column0[1], Column1[1], Column2[1], Column3[1], Column4[1]},
        {Column0[2], Column1[2], Column2[2], Column3[2], Column4[2]},
        {Column0[3], Column1[3], Column2[3], Column3[3], Column4[3]},
        {Column0[4], Column1[4], Column2[4], Column3[4], Column4[4]}
    };
}

FMatrix5::FMatrix5(const FVector5& Row0, const FVector5& Row1, const FVector5& Row2, const FVector5& Row3, const FVector5& Row4)
{
    Rows[0] = Row0;
    Rows[1] = Row1;
    Rows[2] = Row2;
    Rows[3] = Row3;
    Rows[4] = Row4;
}

FMatrix5 operator*(const FMatrix5& A, const FMatrix5& B)
{
    FMatrix5 Result;

    for (int8 I = 0; I < 5; I++)
    {
        for (int8 J = 0; J < 5; J++)
        {
            Result[I][J] = 0.f;

            for (int8 K = 0; K < 5; K++)
                Result[I][J] += A[I][K] * B[K][J];
        }
    }

    return Result;
}

const FVector5& FMatrix5::operator[](int8 Index) const
{
    return Rows[Index];
}

FVector5& FMatrix5::operator[](int8 Index)
{
    return Rows[Index];
}

FMatrix5 operator*(const FMatrix5& A, float B)
{
    FMatrix5 Result;

    for (int8 I = 0; I < 5; I++)
    {
        for (int8 J = 0; J < 5; J++)
        {
            Result[I][J] = A[I][J] * B;
        }
    }

    return Result;
}

FArchive& operator<<(FArchive& Ar, FMatrix5& Matrix)
{
    for (int8 Index = 0; Index < 5; Index++)
        Ar << Matrix.Rows[Index];

    return Ar;
}

FVector5 operator*(const FMatrix5& A, const FVector5& B)
{
    FVector5 Result;

    for (int8 I = 0; I < 5; I++)
    {
        Result[I] = 0.f;

        for (int8 J = 0; J < 5; J++)
            Result[I] += A[I][J] * B[J];
    }

    return Result;
}

FVector5 FMatrix5::GetColumn(int8 Index) const
{
    return {Rows[0][Index], Rows[1][Index], Rows[2][Index], Rows[3][Index], Rows[4][Index]};
}

FVector3f FMatrix5::GetColumn3(int8 Index) const
{
    return {Rows[0][Index], Rows[1][Index], Rows[2][Index]};
}

void FMatrix5::SetColumn(int8 ColumnIndex, const FVector5& Column)
{
    for (int8 Index = 0; Index < 5; Index++)
        Rows[Index][ColumnIndex] = Column[Index];
}
