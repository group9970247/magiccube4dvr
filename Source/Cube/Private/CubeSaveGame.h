#pragma once

#include "Matrix5.h"

#include "GameFramework/SaveGame.h"
#include "Misc/EngineVersionComparison.h"
#include "CubeSaveGame.generated.h"


struct FSubcubeData
{
    FMatrix5 Transform;
    FIntVector4 InitialCell;
};

UCLASS()
class UCubeSaveGame : public USaveGame
{
    GENERATED_BODY()

public:
    virtual void Serialize(FArchive& Ar) override;

    void Save();
    void Load();

    const FString& GetTime() const;
    bool GetSolved() const;

private:
    const uint8 SubcubeCount = 216;


    FString Time;
    bool bSolved;
    TArray<FSubcubeData> Data;
};

void operator<<(FArchive& Ar, FSubcubeData& SubcubeData);
#if UE_VERSION_OLDER_THAN(5, 1, 0)
void operator<<(FArchive& Ar, FIntVector4& IntVector4);
#endif
