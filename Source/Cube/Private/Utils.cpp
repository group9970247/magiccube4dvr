#include "Utils.h"
#include "CubeActor.h"
#include "CubePawn.h"
#include "MenuActor.h"
#include "CubeSettings.h"
#include "CubeGameMode.h"

#include "EngineUtils.h"
#include "Kismet/GameplayStatics.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/KismetSystemLibrary.h"


ACubeGameMode* FUtils::GameMode;
ACubeActor* FUtils::CubeActor;
ACubePawn* FUtils::Pawn;
APlayerController* FUtils::PlayerController;
AMenuActor* FUtils::MenuActor;
FName FUtils::DeviceName;
bool FUtils::bValveIndexController;

void FUtils::Initialize(ACubeGameMode* GameModeIn)
{
    GameMode = GameModeIn;
    bValveIndexController = true;
}

void FUtils::BeginPlay()
{
    UWorld* World = GameMode->GetWorld();

    CubeActor = *TActorIterator<ACubeActor>(World);
    Pawn = Cast<ACubePawn>(UGameplayStatics::GetPlayerPawn(World, 0));
    PlayerController = UGameplayStatics::GetPlayerController(World, 0);

    MenuActor = World->SpawnActor<AMenuActor>(FVector::ZeroVector, FRotator::ZeroRotator);
    MenuActor->SetActorHiddenInGame(true);
}

void FUtils::UpdateContollerType(const FXRMotionControllerData& MotionControllerData)
{
    const FName& NewDeviceName = MotionControllerData.DeviceName;
    if (NewDeviceName == DeviceName)
        return;

    DeviceName = NewDeviceName;
    bValveIndexController = DeviceName.ToString() == TEXT("/interaction_profiles/valve/index_controller");
}

float FUtils::GetAngle(float CosP, float SinP)
{
    float Result = FMath::Acos(CosP);

    if (SinP < 0.f)
        Result = 2.f * PI - Result;

    return Result;
}

float FUtils::GetAngle(const FVector3f& Vector, const FVector3f& E0, const FVector3f& E1)
{
    return GetAngle(FVector3f::DotProduct(Vector, E0), FVector3f::DotProduct(Vector, E1));
}

void FUtils::CosAndSinFromQuarterIndex(int8 Index, float* CosT, float* SinT)
{
    int8 CosTI, SinTI;
    if (Index == 0)
    {
        CosTI = 1;
        SinTI = 0;
    }
    else if (Index == 1)
    {
        CosTI = 0;
        SinTI = 1;
    }
    else if (Index == 2)
    {
        CosTI = -1;
        SinTI = 0;
    }
    else
    {
        CosTI = 0;
        SinTI = -1;
    }

    *CosT = CosTI;
    *SinT = SinTI;
}

void FUtils::GetHeadMatrixWorld(FMatrix44f* HeadMatrixWorld)
{
    FRotator HeadRotationTrackingD;
    FVector HeadPositionTrackingD;
    UHeadMountedDisplayFunctionLibrary::GetOrientationAndPosition(HeadRotationTrackingD, HeadPositionTrackingD);
    FRotator3f HeadRotationTracking(HeadRotationTrackingD);
    FVector3f HeadPositionTracking(HeadPositionTrackingD);

    FTransform3f TrackingToWorld(UHeadMountedDisplayFunctionLibrary::GetTrackingToWorldTransform(Pawn));
    FRotationTranslationMatrix44f HeadMatrixTracking(HeadRotationTracking, HeadPositionTracking);
    *HeadMatrixWorld = HeadMatrixTracking * TrackingToWorld.ToMatrixWithScale();
}

void FUtils::GetOtherAxises(int8 Axis, TArray<int8>* OtherAxises)
{
    for (int8 Index = 0; Index < 3; Index++)
        OtherAxises->Add((Axis + 1 + Index) % 4);
}

ACubeGameMode* FUtils::GetGameMode()
{
    return GameMode;
}

ACubeActor* FUtils::GetCubeActor()
{
    return CubeActor;
}

const FTransform& FUtils::GetCubeTransform()
{
    return CubeActor->GetActorTransform();
}

const FCubeGrid* FUtils::GetConstGrid()
{
    return CubeActor->GetGrid();
}

FCubeGrid* FUtils::GetGrid()
{
    return CubeActor->GetGrid();
}

ACubePawn* FUtils::GetPawn()
{
    return Pawn;
}

APlayerController* FUtils::GetPlayerController()
{
    return PlayerController;
}

AMenuActor* FUtils::GetMenuActor()
{
    return MenuActor;
}

class UMenuWidget* FUtils::GetMenuWidget()
{
    return MenuActor->GetMenu();
}

bool FUtils::InMenu()
{
    return !MenuActor->IsHidden();
}

bool FUtils::ValveIndexController()
{
    return bValveIndexController;
}

void FUtils::PreloadSound(const AActor* Actor, USoundWave* Sound)
{
    PlaySoundInternal(Actor, Sound, 0.001f);
}

void FUtils::PlaySound(const AActor* Actor, USoundWave* Sound)
{
    if (GetMutableDefault<UCubeSettings>()->bSound)
        PlaySoundInternal(Actor, Sound, 1.f);
}

void FUtils::Quit()
{
    UKismetSystemLibrary::QuitGame(GameMode, PlayerController, EQuitPreference::Quit, false);
}

void FUtils::PlaySoundInternal(const AActor* Actor, USoundWave* Sound, float Volume)
{
    UGameplayStatics::PlaySoundAtLocation(Actor, Sound, Actor->GetActorLocation(), Volume);
}
