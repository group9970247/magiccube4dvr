#include "CubeSaveGame.h"
#include "CubeGrid.h"
#include "Subcube.h"
#include "Utils.h"
#include "CubeActor.h"
#include "MenuWidget.h"

void UCubeSaveGame::Serialize(FArchive& Ar)
{
    if (Ar.IsSaving())
    {
        Ar << Time;

        for (uint8 Index = 0; Index < Data.Num(); Index++)
        {
            if (Index == SubcubeCount / 2)
                Ar << bSolved;

            Ar << Data[Index];
        }
    }
    else
    {
        if (!Ar.AtEnd())
            Ar << Time;

        for (uint8 Index = 0; Index < SubcubeCount; Index++)
        {
            if (Index == SubcubeCount / 2)
            {
                if (Ar.AtEnd())
                    break;

                Ar << bSolved;
            }

            if (Ar.AtEnd())
                break;

            Data.Add({FMatrix5::Identity, {0, 0, 0, 0}});
            Ar << Data.Last();
        }

        if (!Ar.AtEnd())
            Data.Add({FMatrix5::Identity, {0, 0, 0, 0}});
    }
}

void UCubeSaveGame::Save()
{
    const UMenuWidget* MenuWidget = FUtils::GetMenuWidget();

    Time = MenuWidget->GetLastTime();
    bSolved = MenuWidget->GetSolved();

    FUtils::GetConstGrid()->ForEach([this](const FSubcube* Subcube)
        {
            Data.Add({Subcube->GetTransform(), Subcube->GetInitialCell()});
        });
}

void UCubeSaveGame::Load()
{
    if (Data.Num() != SubcubeCount)
        return;

    FUtils::GetCubeActor()->ResetGrid();
    FCubeGrid* Grid = FUtils::GetGrid();
    FCubeGrid Copy = *Grid;
    uint8 Count = 0;
    FCubeGrid::ForEach([Grid, &Copy, this, &Count](const FIntVector4& Cell)
        {
            FSubcube*& Subcube = Grid->At(Cell);
            if (!Subcube)
                return;

            const FSubcubeData& SubcubeData = Data[Count++];
            Subcube = Copy.At(SubcubeData.InitialCell);
            Subcube->SetTransform(SubcubeData.Transform);
            Subcube->Update();
            Subcube->SaveTransform();
        });
}

const FString& UCubeSaveGame::GetTime() const
{
    return Time;
}

bool UCubeSaveGame::GetSolved() const
{
    return bSolved;
}

void operator<<(FArchive& Ar, FSubcubeData& SubcubeData)
{
    Ar << SubcubeData.Transform << SubcubeData.InitialCell;
}

#if UE_VERSION_OLDER_THAN(5, 1, 0)
void operator<<(FArchive& Ar, FIntVector4& IntVector4)
{
    for (int8 Index = 0; Index < 4; Index++)
        Ar << IntVector4[Index];
}
#endif
