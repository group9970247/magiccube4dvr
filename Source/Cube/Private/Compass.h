#pragma once

#include "Component4d.h"

#include "Components/PoseableMeshComponent.h"
#include "Compass.generated.h"


class FHandInterface;

UCLASS()
class UCompass : public UPoseableMeshComponent, public FComponent4d
{
    GENERATED_BODY()

public:
    UCompass();
    void SetHandInterface(const FHandInterface* HandInterfaceIn);

    virtual FBoxSphereBounds CalcBounds(const FTransform& LocalToWorld) const override;

    void TickCompass(float DeltaTime, bool bCompassIsActive);
    void SetHand(EControllerHand Hand, const FMatrix44f& CubeToCompass);
    void GrabStart(const FMatrix5& ToGrabbedCube);
    void RotateHandStart();
    void RotateHand(const FMatrix5& SideRotation);
    void ReleaseHand();

protected:
    virtual double GetBoundsRadius(float Matrix44) const override;

private:
    struct FBone
    {
        const FName Name;
        float Target = 0.f;
        float Speed = 0.f;
        bool bSnap = true;
        bool bSnapStartAngleLessThanTarget;


        FBone(const FName& NameIn);
        void SetTarget(float TargetIn, bool bSnapIn);
    };


    USkeletalMesh* CompassLeft;
    USkeletalMesh* CompassRight;

    bool bCompassWasActive = false;

    FBone Platform0 = {"Platform0"};
    FBone Platform1 = {"Platform1"};
    FBone Frame = {"Frame"};
    FBone B0123r0 = {"0123r0"};
    FBone B0123r1 = {"0123r1"};
    FBone B012r2 = {"012r2"};
    FBone B012r0 = {"012r0"};
    FBone B012r1 = {"012r1"};
    FBone B0 = {"0"};

    UPROPERTY()
    UMaterialInstanceDynamic* InstancePreventGC;

    const FHandInterface* HandInterface;
    FMatrix44f DeltaRotation;


    void TickBone(FBone* Bone, float DeltaTime);
    void Ortho(const FVector3f& Vector, const FVector3f& Axis, FVector3f* Result) const;
};
