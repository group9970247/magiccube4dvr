#pragma once

#include "Matrix5.h"

class FRotateSide
{
public:
    static void Start(int8 SideAxisIndex, bool bSideAxisPositive, int8 RotationAxis0In, int8 RotationAxis1In);
    static void Rotate(float CosF, float SinF);
    static void Update();
    static void End(int8 EndAngleI);

    static const FMatrix5& GetSideRotation();
    static float GetAngle();

private:
    static int8 RotationAxis0, RotationAxis1;
    static FIntVector4 RotatingSideStart, RotatingSideEnd;
    static FMatrix5 SideRotation;
};
