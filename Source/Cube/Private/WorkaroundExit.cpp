#include "WorkaroundExit.h"

#include "HAL/RunnableThread.h"
#include "CoreGlobals.h"
#include "Misc/OutputDeviceRedirector.h"

#include <thread>
#if PLATFORM_LINUX
#include <unistd.h>
#endif


void FWorkaroundExit::Execute()
{
#if PLATFORM_LINUX
    FRunnableThread::Create(new FWorkaroundExit, TEXT("WorkaroundExit"));
#endif
}

uint32 FWorkaroundExit::Run()
{
    std::this_thread::sleep_for(std::chrono::seconds(3));

    if (GLog)
        GLog->Flush();

    _exit(0);
    return 0;
}