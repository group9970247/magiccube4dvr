#pragma once

#include "Object4d.h"
#include "Matrix5.h"

class UPrimitiveComponent;
class UMaterial;
class UMaterialInstanceDynamic;

class FComponent4d : public FObject4d
{
public:
    UPrimitiveComponent* GetComponent() const;

protected:
    FComponent4d();
    FComponent4d(UPrimitiveComponent* ComponentIn);
    void SetMaterials(UMaterial* Object4dMaterialIn, UMaterial* Object3dMaterialIn);
    UMaterialInstanceDynamic* CreateObject4dInstance();
    void SetCubeToThis(const FMatrix5& CubeToThisIn);

    void ForceUpdateBounds();
    FBoxSphereBounds CalcBounds4d(const class AActor* Cube) const;

    void EnableObject4dInstance(const FMatrix5& ToGrabbedCube);
    void DisableObject4dInstance();

    virtual double GetBoundsRadius(float Matrix44) const = 0;

    virtual bool UpdateInternal() override;
    virtual void SetRow4(int8 RowIndex) override;
    virtual void SetRow3(int8 RowIndex, float R0, float R1, float R2) override;

private:
    UPrimitiveComponent* Component;

    UMaterial* Object4dMaterial;
    UMaterial* Object3dMaterial;

    UMaterialInstanceDynamic* Object4dInstance;

    FMatrix5 CubeToThis;

    FVector BoundsOriginRelative;
    FVector BoundsBoxExtent;
    double BoundsSphereRadius;
};
