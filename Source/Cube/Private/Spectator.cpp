#include "Spectator.h"
#include "Utils.h"

#include "Components/SceneCaptureComponent2D.h"
#include "Engine/TextureRenderTarget2D.h"
#include "GameFramework/PlayerController.h"
#include "HeadMountedDisplayFunctionLibrary.h"


ASpectator::ASpectator()
{
    RenderTarget = CreateDefaultSubobject<UTextureRenderTarget2D>("RenderTarget");
    RenderTarget->InitCustomFormat(1920, 1080, PF_FloatRGB, false);
    RenderTarget->TargetGamma = 2.2f;

    USceneCaptureComponent2D* Capture = CreateDefaultSubobject<USceneCaptureComponent2D>("RootComponent");
    Capture->TextureTarget = RenderTarget;
    Capture->CaptureSource = ESceneCaptureSource::SCS_FinalColorLDR;
    RootComponent = Capture;

    PrimaryActorTick.TickGroup = TG_PrePhysics;
    PrimaryActorTick.bCanEverTick = true;
}

void ASpectator::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    const APlayerCameraManager* Manager = FUtils::GetPlayerController()->PlayerCameraManager;
    SetActorLocationAndRotation(Manager->GetCameraLocation(), Manager->GetCameraRotation());
}

void ASpectator::Enable()
{
    UHeadMountedDisplayFunctionLibrary::SetSpectatorScreenMode(ESpectatorScreenMode::Texture);
    UHeadMountedDisplayFunctionLibrary::SetSpectatorScreenTexture(RenderTarget);
}
