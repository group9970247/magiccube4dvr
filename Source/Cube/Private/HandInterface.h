#pragma once

#include "Matrix5.h"
#include "Component4d.h"

#include "InputCoreTypes.h"


class UHapticFeedbackEffect_Curve;

enum class EGrabState : uint8
{
    None = 0,
    Move = 1,
    Rotate = 2,
    RotateEnd = 3
};

struct FTryGrabResult
{
    FIntVector4 Cell = FIntVector4(0);
    EGrabState State = EGrabState::None;
    FMatrix5 ToGrabbedCube;
};

class FHandInterface : public FComponent4d
{
public:
    FHandInterface();
    virtual void SetHand(EControllerHand HandIn);

    bool IsTracked() const;

    const FIntVector4& GetGrabbedCell() const;
    EGrabState GetGrabState() const;
    bool IsHolding() const;

    void TryGrab(FTryGrabResult* TryGrabResult) const;
    void Grab(const FTryGrabResult& TryGrabResult);
    void Release();

protected:
    UHapticFeedbackEffect_Curve* GrabCurve;
    UHapticFeedbackEffect_Curve* ReleaseCurve;

    bool bIsTracked = true;

    FIntVector4 GrabbedCell = {0, 0, 0, 0};
    EGrabState GrabState = EGrabState::None;


    FHandInterface(UPrimitiveComponent* ComponentIn, float TooFarOffsetIn, float TooNearOffsetIn);

    void TickHand(float DeltaTime);

    EControllerHand GetHand() const;
    void AttachCubeToHand();

    int8 RoundToQuarterIndex(float P) const;

    virtual bool IsLeft() const = 0;
    virtual void GetGrabPoint(FVector& GrabPoint) const = 0;
    virtual void GetDownVector(FVector3f* Down) const = 0;
    virtual void GetForwardVector(FVector3f* Forward) const = 0;
    virtual void GrabStart(const FMatrix5& ToGrabbedCube) = 0;
    virtual void MoveHandStart(const FMatrix5& ToGrabbedCube) = 0;
    virtual void RotateHandStart(const FMatrix5& ToGrabbedCube);
    virtual void RotateHand(const FMatrix5& SideRotation) = 0;
    virtual void ReleaseHand();

private:
    const float TooFarOffset;
    const float TooNearOffset;

    EControllerHand Hand;

    int8 EndAngleI;
    float EndAngle;
    float EAngleTarget;

    FVector3f GrabAxisAS;
    FVector3f E0GrabStartAS;


    void GetAxisAndBasis(FVector3f* GrabAxis, FVector3f* E0, FVector3f* E1, FVector3f* E2) const;
    void CompletelyEndRotation();
    void PlayHapticEffect(UHapticFeedbackEffect_Curve* Curve);
};
