#pragma once

#include "Object4d.h"

class UInstancedStaticMeshComponent;

class FSubcube : public FObject4d
{
public:
    FSubcube(UInstancedStaticMeshComponent* InstancedStaticMeshComponentIn, const FIntVector4& InitialCellIn, const FLinearColor& Color);

    const FIntVector4& GetInitialCell() const;

    void Reset();
    bool IsVisible() const;

protected:
    virtual bool UpdateInternal() override;
    virtual void SetRow4(int8 RowIndex) override;
    virtual void SetRow3(int8 RowIndex, float R0, float R1, float R2) override;

private:
    UInstancedStaticMeshComponent* Component;

    int32 InstanceIndex;
    FIntVector4 InitialCell;
    const float CullDistance;
    const FTransform HiddenTransform;


    const FMatrix& GetInstanceMatrix() const;
};
