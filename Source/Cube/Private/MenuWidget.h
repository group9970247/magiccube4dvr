#pragma once

#include "Blueprint/UserWidget.h"
#include "MenuWidget.generated.h"

class UCanvasPanelSlot;
class UTextBlock;
class UWidgetSwitcher;

UCLASS()
class UMenuWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    void InitializeIntensity();
    void SelectDefault();
    bool NewGameWasStarted() const;
    void MakeStep(FIntVector2 Step);
    void Press();
    void TickMenu(float DeltaSeconds);

    const FString& GetLastTime() const;
    bool GetSolved() const;

protected:
    UFUNCTION(BlueprintCallable)
    void SetWidgets(const TArray<UWidget*>& Column0, const TArray<UWidget*>& Column1, const TArray<UWidget*>& Column2, const TArray<UWidget*>& Column3, const TArray<UWidget*>& Column4, UBorder* SelectionIn, UWidgetSwitcher* WidgetSwitcherIn);

    UFUNCTION(BlueprintImplementableEvent)
    void InitializeBP();

    UFUNCTION(BlueprintImplementableEvent)
    void IntensityChanged(float NewIntensity);

private:
    TArray<UTextBlock*> SaveSlots;
    TArray<TArray<const UCanvasPanelSlot*>> Buttons;
    UWidget* Continue = (UWidget*)1;
    TArray<UTextBlock*> SettingsValues;
    UCanvasPanelSlot* Selection;
    UWidgetSwitcher* WidgetSwitcher;
    FIntVector2 Selected;
    FString LastTime;
    bool bSolved = false;

    const float IntensityGame = 1.f;
    const float IntensityMenu = 0.17f;
    float Intensity;


    void Select();
    void StartNewGame();
    FString SaveFileName(int32 Index) const;
    void LeaveMenu();
    void UpdateValue(bool bValue, int8 Index);
    void SwitchOption(bool& bOption, int8 Index);
    void SetIntensity(float IntensityIn);
};
