#pragma once

#include "CubeGrid.h"
#include "GameFramework/Actor.h"
#include "CubeActor.generated.h"

class UMaterial;

UCLASS()
class ACubeActor : public AActor
{
    GENERATED_BODY()

public:
    constexpr static float SubcubeSideLength = 6.f;
    constexpr static float MarginLength = 6.f;
    constexpr static float CubeSideLength = (SubcubeSideLength + MarginLength) * 3.f;


    UPROPERTY(EditAnywhere, EditFixedSize)
    TArray<FLinearColor> Colors;


    ACubeActor();
    virtual void PostRegisterAllComponents() override;
    void PreloadSounds();
    ~ACubeActor();

    void PlaySound() const;
    void ResetGrid();
    void UpdateSubcubes();

    const FCubeGrid* GetGrid() const;
    FCubeGrid* GetGrid();

private:
#if WITH_EDITOR
    UMaterial* SubcubeEditorMaterial;
#endif
    UMaterial* SubcubeGameMaterial;

    TArray<class USoundWave*> ClickSounds;
    FCubeGrid Grid;
};
