#include "HandInterface.h"
#include "Subcube.h"
#include "CubeActor.h"
#include "Utils.h"
#include "RotateSide.h"
#include "CubeSettings.h"
#include "Camera.h"
#include "Steam.h"

#include "MatrixTypes.h"
#include "Haptics/HapticFeedbackEffect_Curve.h"
#include "Kismet/GameplayStatics.h"


FHandInterface::FHandInterface() :
    TooFarOffset(0.f),
    TooNearOffset(0.f)
{

}

void FHandInterface::SetHand(EControllerHand HandIn)
{
    Hand = HandIn;
}

bool FHandInterface::IsTracked() const
{
    return bIsTracked;
}

const FIntVector4& FHandInterface::GetGrabbedCell() const
{
    return GrabbedCell;
}

EGrabState FHandInterface::GetGrabState() const
{
    return GrabState;
}

bool FHandInterface::IsHolding() const
{
    return GrabState == EGrabState::Move || GrabState == EGrabState::Rotate;
}

void FHandInterface::TryGrab(FTryGrabResult* TryGrabResult) const
{
    FVector GrabPoint;
    GetGrabPoint(GrabPoint);

    const FTransform& CubeTransform = FUtils::GetCubeTransform();
    FVector3f GrabPointAS(CubeTransform.InverseTransformPositionNoScale(GrabPoint));
    float CubeSideLength = ACubeActor::CubeSideLength;
    if (GrabPointAS.SizeSquared() > FMath::Square(CubeSideLength))
        return;

    const FMatrix5& Projection = FCamera::GetProjection();

    bool bAxisVisible[4];
    bool bAxisPositive[4];
    bool bChangeOrientation = false;
    FVector3f E[4];
    FVector3f Origin = FVector3f::ZeroVector;
    const FCubeGrid* Grid = FUtils::GetConstGrid();
    for (int8 Axis = 0; Axis < 4; Axis++)
    {
        bAxisVisible[Axis] =
            Grid->GetSideCenter(Axis, false)->IsVisible() ||
            Grid->GetSideCenter(Axis, true)->IsVisible();
        bAxisPositive[Axis] = bAxisVisible[Axis] ? Grid->GetSideCenter(Axis, true)->IsVisible() : true;

        if (bAxisPositive[Axis])
            bChangeOrientation = !bChangeOrientation;

        float Sign = bAxisPositive[Axis] ? 1.f : -1.f;
        E[Axis] = Projection.GetColumn3(Axis) * Sign * CubeSideLength;
        Origin += E[Axis];
    }
    Origin /= 2.f;

    FVector3f GrabPointOS = GrabPointAS - Origin;

    for (int8 Axis = 0; Axis < 4; Axis++)
    {
        if (!bAxisVisible[Axis])
            continue;

        TArray<int8> OtherAxises;
        FUtils::GetOtherAxises(Axis, &OtherAxises);

        bool bChangeSideOrientation = Axis % 2 == 0;
        if (bChangeOrientation)
            bChangeSideOrientation = !bChangeSideOrientation;
        if (bChangeSideOrientation)
            Swap(OtherAxises[1], OtherAxises[2]);

        FMatrix44f SideMatrixInverse = FMatrix44f(-E[OtherAxises[0]], -E[OtherAxises[1]], -E[OtherAxises[2]], FVector3f::ZeroVector).InverseFast();
        FVector3f GrabPointSS = SideMatrixInverse.TransformVector(GrabPointOS);

        bool bTooFar = false;
        for (int8 Index = 0; Index < 3; Index++)
        {
            float TooFarMin = bAxisVisible[OtherAxises[Index]] ? 0.f : TooFarOffset;
            float TooFarMax = 1.f - TooFarOffset;

            float Value = GrabPointSS[Index];
            if (Value >= TooFarMin && Value <= TooFarMax)
                continue;

            bTooFar = true;
            break;
        }
        if (bTooFar)
            continue;

        bool bTooNear = true;
        for (int8 Index = 0; Index < 3; Index++)
        {
            float TooNearMin = bAxisVisible[OtherAxises[Index]] ? 0.f : TooNearOffset;
            float TooNearMax = 1.f - TooNearOffset;

            float Value = GrabPointSS[Index];
            if (Value >= TooNearMin && Value <= TooNearMax)
                continue;

            bTooNear = false;
            break;
        }
        if (bTooNear)
            continue;

        FIntVector SideCell;
        for (int8 Index = 0; Index < 3; Index++)
            SideCell[Index] = -(FMath::Clamp(FMath::FloorToInt(GrabPointSS[Index] * 3.f), 0, 2) - 1);

        FIntVector4& Cell = TryGrabResult->Cell;
        Cell[Axis] = 2;
        for (int8 Index = 0; Index < 3; Index++)
            Cell[OtherAxises[Index]] = SideCell[Index];
        for (int8 Index = 0; Index < 4; Index++)
            Cell[Index] *= bAxisPositive[Index] ? 1 : -1;

        EGrabState& TryGrabState = TryGrabResult->State;
        int8 CentersCount = 0;
        for (int8 Index = 0; Index < 4; Index++)
        {
            if (Cell[Index] == 0)
                CentersCount++;
        }
        if (CentersCount == 2)
            TryGrabState = EGrabState::Rotate;
        else
            TryGrabState = EGrabState::Move;


        int8 ScEI[4] = {-1, -1, -1, Axis};

        for (int8 Index = 0; Index < 3; Index++)
        {
            int32 OtherAxisIndex = OtherAxises[Index];
            if (FMath::Abs(Cell[OtherAxisIndex]) == 1)
            {
                ScEI[2] = OtherAxisIndex;
                break;
            }
        }

        TArray<int8> Indices = {0, 1, 2, 3};
        Indices.Remove(ScEI[3]);
        Indices.Remove(ScEI[2]);
        ScEI[0] = Indices[0];
        ScEI[1] = Indices[1];

        bool bSwap = false;
        TArray<int8> ScEICopy(ScEI, 4);
        for (int32 EIndex = 0; EIndex < 3; EIndex++)
        {
            int32 Location;
            for (Location = EIndex; ScEICopy[Location] != EIndex; Location++);

            for (int32 Index = 0; Index < Location - EIndex; Index++)
            {
                Swap(ScEICopy[Location - Index], ScEICopy[Location - Index - 1]);
                bSwap = !bSwap;
            }
        }
        for (int32 Index = 0; Index < 4; Index++)
        {
            if (Cell[ScEI[Index]] < 0)
                bSwap = !bSwap;
        }
        if (bSwap)
            Swap(ScEI[0], ScEI[1]);

        const FSubcube* GrabbedSubcube = Grid->At(Cell);
        FVector5 GrabbedCubeLocation = GrabbedSubcube->GetTransform().GetColumn(4);
        FMatrix5& ToGrabbedCube = TryGrabResult->ToGrabbedCube;

        auto ScE = [ScEI, &Cell](int8 Index)
        {
            int8 ScEIndex = ScEI[Index];
            return FMatrix5::Identity.GetColumn(ScEIndex) * (Cell[ScEIndex] >= 0 ? 1.f : -1.f);
        };

        ToGrabbedCube = FMatrix5::FromColumns(ScE(0), ScE(1), ScE(2), ScE(3), GrabbedCubeLocation);

        FMatrix5 ToGrabbedCubeProjected5 = Projection * ToGrabbedCube;
        UE::Geometry::FMatrix3f ToGrabbedCubeProjectedAS(
            ToGrabbedCubeProjected5.GetColumn3(0),
            ToGrabbedCubeProjected5.GetColumn3(1),
            ToGrabbedCubeProjected5.GetColumn3(2),
            false);

        FMatrix44f CubeMatrix(CubeTransform.ToMatrixNoScale());
        UE::Geometry::FMatrix3f CubeTransform3(
            CubeMatrix.GetScaledAxis(EAxis::X),
            CubeMatrix.GetScaledAxis(EAxis::Y),
            CubeMatrix.GetScaledAxis(EAxis::Z),
            false);

        UE::Geometry::FMatrix3f ToGrabbedCubeProjectedWS = CubeTransform3 * ToGrabbedCubeProjectedAS;
        UE::Geometry::FMatrix3f ToGrabbedCubeProjectedWSInverse = ToGrabbedCubeProjectedWS.Inverse();

        FVector3f Down;
        GetDownVector(&Down);
        FVector3f DownScS = ToGrabbedCubeProjectedWSInverse * Down;
        FVector3f DownScSAbs = DownScS.GetAbs();

        int8 GrabAxisI;
        if (DownScSAbs.X > DownScSAbs.Y)
        {
            if (DownScSAbs.X > DownScSAbs.Z)
                GrabAxisI = 0;
            else
                GrabAxisI = 2;
        }
        else
        {
            if (DownScSAbs.Y > DownScSAbs.Z)
                GrabAxisI = 1;
            else
                GrabAxisI = 2;
        }

        if (FMath::Abs(Cell[ScEI[GrabAxisI]]) != 1 || DownScS[GrabAxisI] > 0.f)
        {
            Cell = FIntVector4(0);
            TryGrabState = EGrabState::None;
            return;
        }

        int8 Axis0I = (GrabAxisI + 1) % 3;
        int8 Axis1I = (GrabAxisI + 2) % 3;

        if (GrabAxisI != 2)
        {
            FVector5 Columns[3] = {
                ToGrabbedCube.GetColumn(0),
                ToGrabbedCube.GetColumn(1),
                ToGrabbedCube.GetColumn(2)
            };
            ToGrabbedCube.SetColumn(2, Columns[GrabAxisI]);
            ToGrabbedCube.SetColumn(0, Columns[Axis0I]);
            ToGrabbedCube.SetColumn(1, Columns[Axis1I]);
        }

        FVector3f Forward;
        GetForwardVector(&Forward);

        FVector3f ForwardScS = ToGrabbedCubeProjectedWSInverse * Forward;
        FVector2f ForwardScS2(ForwardScS[Axis0I], ForwardScS[Axis1I]);
        ForwardScS2.Normalize();

        int8 AngleI = (RoundToQuarterIndex(FUtils::GetAngle(ForwardScS2.X, ForwardScS2.Y)) + 3) % 4;
        float CosP, SinP;
        FUtils::CosAndSinFromQuarterIndex(AngleI, &CosP, &SinP);

        FVector5 ScE0Snap = ToGrabbedCube.GetColumn(0) * CosP + ToGrabbedCube.GetColumn(1) * SinP;
        FVector5 ScE1Snap = -ToGrabbedCube.GetColumn(0) * SinP + ToGrabbedCube.GetColumn(1) * CosP;

        ToGrabbedCube.SetColumn(0, ScE0Snap);
        ToGrabbedCube.SetColumn(1, ScE1Snap);

        return;
    }
}

void FHandInterface::Grab(const FTryGrabResult& TryGrabResult)
{
    PlayHapticEffect(GrabCurve);

    GrabbedCell = TryGrabResult.Cell;
    GrabState = TryGrabResult.State;

    const FMatrix5& ToGrabbedCube = TryGrabResult.ToGrabbedCube;
    GrabStart(ToGrabbedCube);

    if (GrabState == EGrabState::Move)
        MoveHandStart(ToGrabbedCube);
    else
    {
        int8 GrabAxisIndex = -1;
        int8 RotationAxis0 = -1;
        int8 RotationAxis1 = -1;
        int8 SideAxisIndex = -1;
        for (int8 Index = 0; Index < 4; Index++)
        {
            if (FMath::Abs(GrabbedCell[Index]) == 1)
                GrabAxisIndex = Index;
            else if (GrabbedCell[Index] == 0)
            {
                if (RotationAxis0 == -1)
                    RotationAxis0 = Index;
                else
                    RotationAxis1 = Index;
            }
            else
                SideAxisIndex = Index;
        }

        const FMatrix5& Projection = FCamera::GetProjection();
        GrabAxisAS = (-Projection.GetColumn3(GrabAxisIndex) * GrabbedCell[GrabAxisIndex]).GetSafeNormal();
        if (Projection.GetColumn3(RotationAxis0).Cross(Projection.GetColumn3(RotationAxis1)).Dot(GrabAxisAS) < 0.f)
            Swap(RotationAxis0, RotationAxis1);

        FRotateSide::Start(SideAxisIndex, GrabbedCell[SideAxisIndex] == 2, RotationAxis0, RotationAxis1);

        FVector3f GrabAxis, E0, E1, E2;
        GetAxisAndBasis(&GrabAxis, &E0, &E1, &E2);

        E0GrabStartAS = FVector3f(FUtils::GetCubeTransform().InverseTransformVectorNoScale(FVector(E0)));
        RotateHandStart(ToGrabbedCube);
    }
}

void FHandInterface::Release()
{
    PlayHapticEffect(ReleaseCurve);

    GrabbedCell = FIntVector4(0);

    if (GrabState == EGrabState::Move)
    {
        GrabState = EGrabState::None;
        FUtils::GetCubeActor()->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
    }
    else
    {
        EndAngle = FRotateSide::GetAngle();
        EndAngleI = RoundToQuarterIndex(EndAngle);
        EAngleTarget = EndAngleI * HALF_PI;
        EndAngleI %= 4;

        if (FMath::IsNearlyEqual(EndAngle, EAngleTarget))
            CompletelyEndRotation();
        else
            GrabState = EGrabState::RotateEnd;
    }

    ReleaseHand();
}

FHandInterface::FHandInterface(UPrimitiveComponent* ComponentIn, float TooFarOffsetIn, float TooNearOffsetIn) :
    FComponent4d(ComponentIn),
    TooFarOffset(TooFarOffsetIn),
    TooNearOffset(TooNearOffsetIn)
{

}

void FHandInterface::TickHand(float DeltaTime)
{
    if (GrabState == EGrabState::Rotate)
    {
        FVector3f GrabAxis, E0, E1, E2;
        GetAxisAndBasis(&GrabAxis, &E0, &E1, &E2);

        FVector3f E0GrabStart(FUtils::GetCubeTransform().TransformVectorNoScale(FVector(E0GrabStartAS)));
        FRotateSide::Rotate(E0GrabStart.Dot(E0), E0GrabStart.Cross(E0).Dot(GrabAxis));
        FRotateSide::Update();
        RotateHand(FRotateSide::GetSideRotation());
    }
    else if (GrabState == EGrabState::RotateEnd)
    {
        float Sign = FMath::Sign(EAngleTarget - EndAngle);
        float RotateEndFrequency = 2.f;
        EndAngle += Sign * RotateEndFrequency * DeltaTime;
        if ((EAngleTarget - EndAngle) * Sign > 0.f)
        {
            float CosF, SinF;
            FMath::SinCos(&SinF, &CosF, EndAngle);

            FRotateSide::Rotate(CosF, SinF);
            FRotateSide::Update();
        }
        else
            CompletelyEndRotation();
    }
}

EControllerHand FHandInterface::GetHand() const
{
    return Hand;
}

void FHandInterface::AttachCubeToHand()
{
    FUtils::GetCubeActor()->AttachToComponent(GetComponent(), FAttachmentTransformRules::KeepWorldTransform);
}

int8 FHandInterface::RoundToQuarterIndex(float P) const
{
    return FMath::RoundToInt(P / HALF_PI);
}

void FHandInterface::RotateHandStart(const FMatrix5& ToGrabbedCube)
{

}

void FHandInterface::ReleaseHand()
{

}

void FHandInterface::GetAxisAndBasis(FVector3f* GrabAxis, FVector3f* E0, FVector3f* E1, FVector3f* E2) const
{
    *GrabAxis = FVector3f(FUtils::GetCubeTransform().TransformVectorNoScale(FVector(GrabAxisAS)));
    *E1 = (*GrabAxis) * (IsLeft() ? 1.f : -1.f);

    FVector3f Forward;
    GetForwardVector(&Forward);
    *E2 = Forward.Cross(*E1).GetSafeNormal();

    *E0 = E1->Cross(*E2);
}

void FHandInterface::CompletelyEndRotation()
{
    FUtils::GetCubeActor()->PlaySound();
    FRotateSide::End(EndAngleI);
    FRotateSide::Update();
    GrabState = EGrabState::None;
    FSteam::CommitGridState();
}

void FHandInterface::PlayHapticEffect(UHapticFeedbackEffect_Curve* Curve)
{
    if (GetMutableDefault<UCubeSettings>()->bHaptics)
        FUtils::GetPlayerController()->PlayHapticEffect(Curve, Hand);
}
