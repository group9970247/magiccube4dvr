#include "CubeGameMode.h"
#include "CubePawn.h"
#include "Utils.h"
#include "CubeActor.h"
#include "MenuActor.h"
#include "MenuWidget.h"
#include "Camera.h"
#include "Steam.h"
#include "WorkaroundExit.h"

#include "Components/SkeletalMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Misc/MessageDialog.h"


ACubeGameMode::ACubeGameMode()
{
    struct FConstructorStatics
    {
        ConstructorHelpers::FObjectFinder<USkeletalMesh> Left;
        ConstructorHelpers::FObjectFinder<UAnimSequence> LeftGrab;

        ConstructorHelpers::FObjectFinder<USkeletalMesh> Right;
        ConstructorHelpers::FObjectFinder<UAnimSequence> RightGrab;


        FConstructorStatics() :
            Left(TEXT("/Game/Left")),
            LeftGrab(TEXT("/Game/Left_Anim_LeftGrab")),

            Right(TEXT("/Game/Right")),
            RightGrab(TEXT("/Game/Right_Anim_RightGrab"))
        {

        }
    };
    static FConstructorStatics ConstructorStatics;

    PrimaryActorTick.TickGroup = TG_PrePhysics;
    PrimaryActorTick.bCanEverTick = true;

    FUtils::Initialize(this);
    FCamera::Initialize();


    SkeletalMeshLeft = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMeshLeft");
    SkeletalMeshLeft->SetHiddenInGame(true);
    SkeletalMeshLeft->SetSkeletalMesh(ConstructorStatics.Left.Object);
    LeftGrab = ConstructorStatics.LeftGrab.Object;

    SkeletalMeshRight = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMeshRight");
    SkeletalMeshRight->SetHiddenInGame(true);
    SkeletalMeshRight->SetSkeletalMesh(ConstructorStatics.Right.Object);
    RightGrab = ConstructorStatics.RightGrab.Object;
}

USkeletalMeshComponent* ACubeGameMode::GetSkeletalMesh(EControllerHand Hand) const
{
    return Hand == EControllerHand::Left ? SkeletalMeshLeft : SkeletalMeshRight;
}

void ACubeGameMode::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    if (TickIndex < 2)
    {
        if (TickIndex == 0)
        {
            if (!UHeadMountedDisplayFunctionLibrary::HasValidTrackingPosition())
                return;

            TickIndex++;
            UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
            return;
        }

        TickIndex++;
        FUtils::GetPawn()->FirstTick();
        FUtils::GetCubeActor()->PreloadSounds();
        FUtils::GetMenuActor()->Initialize();
    }

    FCamera::Tick(DeltaSeconds);
    FUtils::GetMenuWidget()->TickMenu(DeltaSeconds);
}

void ACubeGameMode::BeginPlay()
{
    Super::BeginPlay();

    if (!UHeadMountedDisplayFunctionLibrary::GetVersionString().Contains("SteamVR"))
    {
        FText Title = FText::FromString("OpenXR Runtime Error");
        FMessageDialog::Open(EAppMsgType::Ok, FText::FromString("SteamVR is not set as default OpenXR runtime!\nPlease open SteamVR settings, enable \"Advanced Settings\", click \"Developer\", then click \"SET STEAMVR AS OPENXR RUNTIME\"."), &Title);
        FUtils::Quit();
        return;
    }

    FSteam::Initialize();


    SkeletalMeshLeft->PlayAnimation(LeftGrab, true);
    SkeletalMeshLeft->Stop();

    SkeletalMeshRight->PlayAnimation(RightGrab, true);
    SkeletalMeshRight->Stop();
}

void ACubeGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    FSteam::Deinitialize();
    Super::EndPlay(EndPlayReason);
    FWorkaroundExit::Execute();
}
