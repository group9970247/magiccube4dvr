#include "CubePawn.h"
#include "CubeMotionControllerComponent.h"
#include "RiggedHand.h"
#include "CubeActor.h"
#include "MenuActor.h"
#include "MenuWidget.h"
#include "Utils.h"
#include "Camera.h"
#include "Spectator.h"

#include "EnhancedInputComponent.h"
#include "UObject/ConstructorHelpers.h"


ACubePawn::ACubePawn()
{
    struct FConstructorStatics
    {
        ConstructorHelpers::FObjectFinder<UInputAction> FrameCube;

        ConstructorHelpers::FObjectFinder<UInputAction> Rotate012Left;
        ConstructorHelpers::FObjectFinder<UInputAction> Rotate23Left;

        ConstructorHelpers::FObjectFinder<UInputAction> Rotate012Right;
        ConstructorHelpers::FObjectFinder<UInputAction> Rotate23Right;

        ConstructorHelpers::FObjectFinder<UInputAction> GrabLeft;
        ConstructorHelpers::FObjectFinder<UInputAction> GrabRight;

        ConstructorHelpers::FObjectFinder<UInputAction> MenuLeft;
        ConstructorHelpers::FObjectFinder<UInputAction> MenuRight;

        ConstructorHelpers::FObjectFinder<UInputAction> Menu;
        ConstructorHelpers::FObjectFinder<UInputAction> Press;

        ConstructorHelpers::FObjectFinder<UInputAction> OculusThumbstickButtonLeft;
        ConstructorHelpers::FObjectFinder<UInputAction> OculusRotate012Left;
        ConstructorHelpers::FObjectFinder<UInputAction> OculusRotate23Left;

        ConstructorHelpers::FObjectFinder<UInputAction> OculusThumbstickButtonRight;
        ConstructorHelpers::FObjectFinder<UInputAction> OculusRotate012Right;
        ConstructorHelpers::FObjectFinder<UInputAction> OculusRotate23Right;


        FConstructorStatics() :
            FrameCube(TEXT("/Game/InputActions/FrameCube")),

            Rotate012Left(TEXT("/Game/InputActions/Rotate012Left")),
            Rotate23Left(TEXT("/Game/InputActions/Rotate23Left")),

            Rotate012Right(TEXT("/Game/InputActions/Rotate012Right")),
            Rotate23Right(TEXT("/Game/InputActions/Rotate23Right")),

            GrabLeft(TEXT("/Game/InputActions/GrabLeft")),
            GrabRight(TEXT("/Game/InputActions/GrabRight")),

            MenuLeft(TEXT("/Game/InputActions/MenuLeft")),
            MenuRight(TEXT("/Game/InputActions/MenuRight")),

            Menu(TEXT("/Game/InputActions/Menu")),
            Press(TEXT("/Game/InputActions/Press")),

            OculusThumbstickButtonLeft(TEXT("/Game/InputActions/OculusThumbstickButtonLeft")),
            OculusRotate012Left(TEXT("/Game/InputActions/OculusRotate012Left")),
            OculusRotate23Left(TEXT("/Game/InputActions/OculusRotate23Left")),

            OculusThumbstickButtonRight(TEXT("/Game/InputActions/OculusThumbstickButtonRight")),
            OculusRotate012Right(TEXT("/Game/InputActions/OculusRotate012Right")),
            OculusRotate23Right(TEXT("/Game/InputActions/OculusRotate23Right"))
        {

        }
    };
    static FConstructorStatics ConstructorStatics;


    RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");

    if (bHandTracking)
        CreateHands<URiggedHand>();
    else
        CreateHands<UCubeMotionControllerComponent>();


    FrameCube = ConstructorStatics.FrameCube.Object;

    Rotate012Left = ConstructorStatics.Rotate012Left.Object;
    Rotate23Left = ConstructorStatics.Rotate23Left.Object;

    Rotate012Right = ConstructorStatics.Rotate012Right.Object;
    Rotate23Right = ConstructorStatics.Rotate23Right.Object;

    GrabLeft = ConstructorStatics.GrabLeft.Object;
    GrabRight = ConstructorStatics.GrabRight.Object;

    MenuLeft = ConstructorStatics.MenuLeft.Object;
    MenuRight = ConstructorStatics.MenuRight.Object;

    Menu = ConstructorStatics.Menu.Object;
    Press = ConstructorStatics.Press.Object;

    OculusThumbstickButtonLeft = ConstructorStatics.OculusThumbstickButtonLeft.Object;
    OculusRotate012Left = ConstructorStatics.OculusRotate012Left.Object;
    OculusRotate23Left = ConstructorStatics.OculusRotate23Left.Object;

    OculusThumbstickButtonRight = ConstructorStatics.OculusThumbstickButtonRight.Object;
    OculusRotate012Right = ConstructorStatics.OculusRotate012Right.Object;
    OculusRotate23Right = ConstructorStatics.OculusRotate23Right.Object;
}

const FHandInterface* ACubePawn::GetCameraHand() const
{
    return CameraHand;
}

void ACubePawn::ResetCameraHand()
{
    CameraHand = nullptr;
}

void ACubePawn::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    auto CheckIfTracked = [DeltaSeconds, this](FHandInterface* Hand)
    {
        if (Hand->IsTracked())
            return;

        if (CameraHand == Hand)
            StopCameraRotation();
    };

    CheckIfTracked(LeftHand);
    CheckIfTracked(RightHand);
}

void ACubePawn::BeginPlay()
{
    Super::BeginPlay();
    FUtils::BeginPlay();
    FCamera::Reset();
}

void ACubePawn::PostRegisterAllComponents()
{
    LeftHand->SetHand(EControllerHand::Left);
    RightHand->SetHand(EControllerHand::Right);
}

void ACubePawn::FirstTick()
{
    FVector3f Position;
    GetFramedCubePosition(&Position);
    FrameCubeToPosition({Position.X, Position.Y, DefaultHeight});

    MenuStarted();

    GetWorld()->SpawnActor<ASpectator>(FVector::ZeroVector, FRotator::ZeroRotator)->Enable();
}

void ACubePawn::MenuStarted()
{
    if (!FUtils::InMenu())
    {
        if (LeftHand->IsHolding())
            LeftHand->Release();
        if (RightHand->IsHolding())
            RightHand->Release();

        StopCameraRotation();
    }

    FUtils::GetMenuActor()->SwitchMenu();
}

void ACubePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    UEnhancedInputComponent* Component = Cast<UEnhancedInputComponent>(PlayerInputComponent);

    Component->BindAction(FrameCube, ETriggerEvent::Started, this, &ACubePawn::FrameCubeStarted);

    Component->BindAction(Rotate012Left, ETriggerEvent::Triggered, this, &ACubePawn::Rotate012LeftTriggered);
    Component->BindAction(Rotate012Left, ETriggerEvent::Completed, this, &ACubePawn::Rotate012LeftCompleted);

    Component->BindAction(Rotate23Left, ETriggerEvent::Triggered, this, &ACubePawn::Rotate23LeftTriggered);
    Component->BindAction(Rotate23Left, ETriggerEvent::Completed, this, &ACubePawn::Rotate23LeftCompleted);

    Component->BindAction(Rotate012Right, ETriggerEvent::Triggered, this, &ACubePawn::Rotate012RightTriggered);
    Component->BindAction(Rotate012Right, ETriggerEvent::Completed, this, &ACubePawn::Rotate012RightCompleted);

    Component->BindAction(Rotate23Right, ETriggerEvent::Triggered, this, &ACubePawn::Rotate23RightTriggered);
    Component->BindAction(Rotate23Right, ETriggerEvent::Completed, this, &ACubePawn::Rotate23RightCompleted);

    Component->BindAction(GrabLeft, ETriggerEvent::Started, this, &ACubePawn::GrabLeftStarted);
    Component->BindAction(GrabLeft, ETriggerEvent::Completed, this, &ACubePawn::GrabLeftCompleted);

    Component->BindAction(GrabRight, ETriggerEvent::Started, this, &ACubePawn::GrabRightStarted);
    Component->BindAction(GrabRight, ETriggerEvent::Completed, this, &ACubePawn::GrabRightCompleted);

    Component->BindAction(MenuLeft, ETriggerEvent::Started, this, &ACubePawn::MenuLeftStarted);
    Component->BindAction(MenuRight, ETriggerEvent::Started, this, &ACubePawn::MenuRightStarted);

    Component->BindAction(Menu, ETriggerEvent::Started, this, &ACubePawn::MenuStarted);
    Component->BindAction(Press, ETriggerEvent::Started, this, &ACubePawn::PressStarted);

    Component->BindAction(OculusThumbstickButtonLeft, ETriggerEvent::Started, this, &ACubePawn::OculusThumbstickButtonLeftStarted);
    Component->BindAction(OculusThumbstickButtonLeft, ETriggerEvent::Completed, this, &ACubePawn::OculusThumbstickButtonLeftCompleted);

    Component->BindAction(OculusRotate012Left, ETriggerEvent::Triggered, this, &ACubePawn::OculusRotate012LeftTriggered);
    Component->BindAction(OculusRotate012Left, ETriggerEvent::Completed, this, &ACubePawn::Rotate012LeftCompleted);

    Component->BindAction(OculusRotate23Left, ETriggerEvent::Triggered, this, &ACubePawn::OculusRotate23LeftTriggered);
    Component->BindAction(OculusRotate23Left, ETriggerEvent::Completed, this, &ACubePawn::Rotate23LeftCompleted);

    Component->BindAction(OculusThumbstickButtonRight, ETriggerEvent::Started, this, &ACubePawn::OculusThumbstickButtonRightStarted);
    Component->BindAction(OculusThumbstickButtonRight, ETriggerEvent::Completed, this, &ACubePawn::OculusThumbstickButtonRightCompleted);

    Component->BindAction(OculusRotate012Right, ETriggerEvent::Triggered, this, &ACubePawn::OculusRotate012RightTriggered);
    Component->BindAction(OculusRotate012Right, ETriggerEvent::Completed, this, &ACubePawn::Rotate012RightCompleted);

    Component->BindAction(OculusRotate23Right, ETriggerEvent::Triggered, this, &ACubePawn::OculusRotate23RightTriggered);
    Component->BindAction(OculusRotate23Right, ETriggerEvent::Completed, this, &ACubePawn::Rotate23RightCompleted);
}

void ACubePawn::FrameCubeStarted()
{
    if (FUtils::InMenu())
        return;

    FVector3f Position;
    GetFramedCubePosition(&Position);
    FrameCubeToPosition(Position);
}

void ACubePawn::Rotate012LeftTriggered(const FInputActionInstance& Instance)
{
    Rotate012Triggered(LeftHand, Instance);
}

void ACubePawn::Rotate012LeftCompleted(const FInputActionInstance& Instance)
{
    if (FUtils::InMenu())
        return;

    RotateCamera(LeftHand, ECameraRotationPlane::Plane01, 0.f);
    RotateCamera(LeftHand, ECameraRotationPlane::Plane12, 0.f);
}

void ACubePawn::Rotate23LeftTriggered(const FInputActionInstance& Instance)
{
    Rotate23Triggered(LeftHand, Instance);
}

void ACubePawn::Rotate23LeftCompleted(const FInputActionInstance& Instance)
{
    if (!FUtils::InMenu())
        RotateCamera(LeftHand, ECameraRotationPlane::Plane23, 0.f);
}

void ACubePawn::Rotate012RightTriggered(const FInputActionInstance& Instance)
{
    Rotate012Triggered(RightHand, Instance);
}

void ACubePawn::Rotate012RightCompleted(const FInputActionInstance& Instance)
{
    if (FUtils::InMenu())
        return;

    RotateCamera(RightHand, ECameraRotationPlane::Plane01, 0.f);
    RotateCamera(RightHand, ECameraRotationPlane::Plane12, 0.f);
}

void ACubePawn::Rotate23RightTriggered(const FInputActionInstance& Instance)
{
    Rotate23Triggered(RightHand, Instance);
}

void ACubePawn::Rotate23RightCompleted(const FInputActionInstance& Instance)
{
    if (!FUtils::InMenu())
        RotateCamera(RightHand, ECameraRotationPlane::Plane23, 0.f);
}

void ACubePawn::GrabLeftStarted()
{
    Grab(LeftHand);
}

void ACubePawn::GrabLeftCompleted()
{
    Release(LeftHand);
}

void ACubePawn::GrabRightStarted()
{
    Grab(RightHand);
}

void ACubePawn::GrabRightCompleted()
{
    Release(RightHand);
}

void ACubePawn::MenuLeftStarted(const FInputActionInstance& Instance)
{
    MenuStep(LeftHand, Instance);
}

void ACubePawn::MenuRightStarted(const FInputActionInstance& Instance)
{
    MenuStep(RightHand, Instance);
}

void ACubePawn::PressStarted()
{
    if (FUtils::InMenu())
        FUtils::GetMenuWidget()->Press();
}

void ACubePawn::OculusThumbstickButtonLeftStarted(const FInputActionInstance& Instance)
{
    bOculusThumbstickLeftPressed = true;
    Rotate012LeftCompleted(Instance);
}

void ACubePawn::OculusThumbstickButtonLeftCompleted(const FInputActionInstance& Instance)
{
    bOculusThumbstickLeftPressed = false;
    Rotate23LeftCompleted(Instance);
}

void ACubePawn::OculusRotate012LeftTriggered(const FInputActionInstance& Instance)
{
    if (!bOculusThumbstickLeftPressed)
        Rotate012LeftTriggered(Instance);
}

void ACubePawn::OculusRotate23LeftTriggered(const FInputActionInstance& Instance)
{
    if (bOculusThumbstickLeftPressed)
        Rotate23LeftTriggered(Instance);
}

void ACubePawn::OculusThumbstickButtonRightStarted(const FInputActionInstance& Instance)
{
    bOculusThumbstickRightPressed = true;
    Rotate012RightCompleted(Instance);
}

void ACubePawn::OculusThumbstickButtonRightCompleted(const FInputActionInstance& Instance)
{
    bOculusThumbstickRightPressed = false;
    Rotate23RightCompleted(Instance);
}

void ACubePawn::OculusRotate012RightTriggered(const FInputActionInstance& Instance)
{
    if (!bOculusThumbstickRightPressed)
        Rotate012RightTriggered(Instance);
}

void ACubePawn::OculusRotate23RightTriggered(const FInputActionInstance& Instance)
{
    if (bOculusThumbstickRightPressed)
        Rotate23RightTriggered(Instance);
}

void ACubePawn::Rotate012Triggered(const FHandInterface* Hand, const FInputActionInstance& Instance)
{
    if (FUtils::InMenu() || !Hand->IsTracked())
        return;

    FVector2D Value = Instance.GetValue().Get<FVector2D>();

    RotateCamera(Hand, ECameraRotationPlane::Plane01, Value.X);
    RotateCamera(Hand, ECameraRotationPlane::Plane12, Value.Y);
}

void ACubePawn::Rotate23Triggered(const FHandInterface* Hand, const FInputActionInstance& Instance)
{
    if (!FUtils::InMenu() && Hand->IsTracked())
        RotateCamera(Hand, ECameraRotationPlane::Plane23, Instance.GetValue().Get<float>());
}

void ACubePawn::Grab(FHandInterface* Hand)
{
    if (FUtils::InMenu() || FCamera::RotationIsActive())
        return;

    if (Hand->GetGrabState() != EGrabState::None)
        return;

    FTryGrabResult TryGrabResult;
    Hand->TryGrab(&TryGrabResult);
    EGrabState GrabState = TryGrabResult.State;
    if (GrabState == EGrabState::None)
        return;

    FHandInterface* OtherHand = Hand == LeftHand ? RightHand : LeftHand;
    EGrabState OtherGrabState = OtherHand->GetGrabState();

    if (OtherGrabState == EGrabState::Move)
    {
        if (GrabState == EGrabState::Move)
            OtherHand->Release();
        else if (GrabState == EGrabState::Rotate)
        {
            if (CellIsInRotatingPart(OtherHand->GetGrabbedCell(), TryGrabResult.Cell))
                OtherHand->Release();
        }
    }
    else if (OtherGrabState == EGrabState::Rotate || OtherGrabState == EGrabState::RotateEnd)
    {
        if (GrabState == EGrabState::Move)
        {
            if (CellIsInRotatingPart(TryGrabResult.Cell, OtherHand->GetGrabbedCell()))
                return;
        }
        else if (GrabState == EGrabState::Rotate)
            return;
    }

    FCamera::Reset();
    Hand->Grab(TryGrabResult);
}

void ACubePawn::Release(FHandInterface* Hand)
{
    if (FUtils::InMenu() || FCamera::RotationIsActive())
        return;

    if (!Hand->IsHolding())
        return;

    Hand->Release();
}

void ACubePawn::MenuStep(const FHandInterface* Hand, const FInputActionInstance& Instance)
{
    if (!FUtils::InMenu() || !Hand->IsTracked())
        return;

    FVector2D StepF = Instance.GetValue().Get<FVector2D>();
    FIntVector2 Step;
    if (FMath::Abs(StepF.X) > FMath::Abs(StepF.Y))
        Step = {StepF.X > 0 ? 1 : -1, 0};
    else
        Step = {0, StepF.Y > 0 ? -1 : 1};

    FUtils::GetMenuWidget()->MakeStep(Step);
}

void ACubePawn::RotateCamera(const FHandInterface* Hand, ECameraRotationPlane CameraRotationPlane, float Value)
{
    bool bActive = FCamera::RotationIsActive();

    if (bActive)
    {
        if (Hand != CameraHand)
            return;
    }
    else
    {
        if (LeftHand->IsHolding() || RightHand->IsHolding())
            return;
    }

    FCamera::Rotate(CameraRotationPlane, Value);

    if (!bActive)
        CameraHand = Hand;
}

bool ACubePawn::CellIsInRotatingPart(const FIntVector4& Cell, const FIntVector4& RotatingCell) const
{
    int8 SideAxisIndex;
    for (SideAxisIndex = 0; SideAxisIndex < 4 && FMath::Abs(RotatingCell[SideAxisIndex]) != 2; SideAxisIndex++);

    int8 Min, Max;
    if (RotatingCell[SideAxisIndex] == 2)
    {
        Min = 1;
        Max = 3;
    }
    else
    {
        Min = -2;
        Max = 0;
    }

    return Cell[SideAxisIndex] >= Min && Cell[SideAxisIndex] < Max;
}

template <typename T>
void ACubePawn::CreateHands()
{
    T* Left = CreateDefaultSubobject<T>("LeftHand");
    LeftHand = static_cast<FHandInterface*>(Left);
    LeftHand->GetComponent()->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

    T* Right = CreateDefaultSubobject<T>("RightHand");
    RightHand = static_cast<FHandInterface*>(Right);
    RightHand->GetComponent()->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

void ACubePawn::StopCameraRotation()
{
    if (!FCamera::RotationIsActive())
        return;

    RotateCamera(CameraHand, ECameraRotationPlane::Plane01, 0.f);
    RotateCamera(CameraHand, ECameraRotationPlane::Plane12, 0.f);
    RotateCamera(CameraHand, ECameraRotationPlane::Plane23, 0.f);
}

void ACubePawn::GetFramedCubePosition(FVector3f* Position) const
{
    FMatrix44f HeadMatrixWorld;
    FUtils::GetHeadMatrixWorld(&HeadMatrixWorld);

    *Position = HeadMatrixWorld.TransformPosition({75.f, 0.f, 0.f});
}

void ACubePawn::FrameCubeToPosition(const FVector3f& Position)
{
    FUtils::GetCubeActor()->SetActorTransform(FTransform(FVector(Position)));
}
