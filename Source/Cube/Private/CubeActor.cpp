#include "CubeActor.h"
#include "Subcube.h"
#include "Utils.h"

#include "UObject/ConstructorHelpers.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"


ACubeActor::ACubeActor()
{
    struct FConstructorStatics
    {
        ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMesh;

#if WITH_EDITOR
        ConstructorHelpers::FObjectFinder<UMaterial> SubcubeEditorMaterial;
#endif
        ConstructorHelpers::FObjectFinder<UMaterial> SubcubeGameMaterial;

        ConstructorHelpers::FObjectFinder<USoundWave> Click00;
        ConstructorHelpers::FObjectFinder<USoundWave> Click01;
        ConstructorHelpers::FObjectFinder<USoundWave> Click02;
        ConstructorHelpers::FObjectFinder<USoundWave> Click03;
        ConstructorHelpers::FObjectFinder<USoundWave> Click04;
        ConstructorHelpers::FObjectFinder<USoundWave> Click05;
        ConstructorHelpers::FObjectFinder<USoundWave> Click06;
        ConstructorHelpers::FObjectFinder<USoundWave> Click07;
        ConstructorHelpers::FObjectFinder<USoundWave> Click08;
        ConstructorHelpers::FObjectFinder<USoundWave> Click09;
        ConstructorHelpers::FObjectFinder<USoundWave> Click10;


        FConstructorStatics() :
            StaticMesh(TEXT("/Game/Cube")),

#if WITH_EDITOR
            SubcubeEditorMaterial(TEXT("/Game/SubcubeEditorMaterial")),
#endif
            SubcubeGameMaterial(TEXT("/Game/SubcubeGameMaterial")),

            Click00(TEXT("/Game/ClickSounds/Click00")),
            Click01(TEXT("/Game/ClickSounds/Click01")),
            Click02(TEXT("/Game/ClickSounds/Click02")),
            Click03(TEXT("/Game/ClickSounds/Click03")),
            Click04(TEXT("/Game/ClickSounds/Click04")),
            Click05(TEXT("/Game/ClickSounds/Click05")),
            Click06(TEXT("/Game/ClickSounds/Click06")),
            Click07(TEXT("/Game/ClickSounds/Click07")),
            Click08(TEXT("/Game/ClickSounds/Click08")),
            Click09(TEXT("/Game/ClickSounds/Click09")),
            Click10(TEXT("/Game/ClickSounds/Click10"))
        {

        }
    };
    static FConstructorStatics ConstructorStatics;

    Colors.SetNumZeroed(8);

#if WITH_EDITOR
    SubcubeEditorMaterial = ConstructorStatics.SubcubeEditorMaterial.Object;
#endif
    SubcubeGameMaterial = ConstructorStatics.SubcubeGameMaterial.Object;

    ClickSounds = {
        ConstructorStatics.Click00.Object,
        ConstructorStatics.Click01.Object,
        ConstructorStatics.Click02.Object,
        ConstructorStatics.Click03.Object,
        ConstructorStatics.Click04.Object,
        ConstructorStatics.Click05.Object,
        ConstructorStatics.Click06.Object,
        ConstructorStatics.Click07.Object,
        ConstructorStatics.Click08.Object,
        ConstructorStatics.Click09.Object,
        ConstructorStatics.Click10.Object
    };

    UInstancedStaticMeshComponent* InstancedStaticMeshComponent = Cast<UInstancedStaticMeshComponent>(CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>("RootComponent"));
    RootComponent = InstancedStaticMeshComponent;
    InstancedStaticMeshComponent->SetStaticMesh(ConstructorStatics.StaticMesh.Object);
#if WITH_EDITOR
    InstancedStaticMeshComponent->bVisualizeComponent = true;
#endif
}

void ACubeActor::PostRegisterAllComponents()
{
    if (!UGameplayStatics::GetGameMode(this))
        return;

    Grid.ForEach([](FSubcube* Subcube)
        {
            delete Subcube;
        });

    UInstancedStaticMeshComponent* InstancedStaticMeshComponent = Cast<UInstancedStaticMeshComponent>(RootComponent);

    InstancedStaticMeshComponent->ClearInstances();

    int32 NumCustomDataFloats;
    UMaterial* Material;

#if WITH_EDITOR
    bool bInGame = GetWorld() && GetWorld()->IsGameWorld();

    if (bInGame)
    {
#endif
        NumCustomDataFloats = 21;
        Material = SubcubeGameMaterial;
#if WITH_EDITOR
    }
    else
    {
        NumCustomDataFloats = 3;
        Material = SubcubeEditorMaterial;
    }
#endif

    InstancedStaticMeshComponent->NumCustomDataFloats = NumCustomDataFloats;
    InstancedStaticMeshComponent->SetMaterial(0, Material);
    InstancedStaticMeshComponent->SetCullDistances(0, 1000);

    for (int32 SideIndex = 0; SideIndex < 8; SideIndex++)
    {
        int8 Axis = SideIndex / 2;
        bool bPositive = SideIndex % 2 == 0;

        auto GetStartIndexComponent = [Axis, bPositive](int8 Index)
        {
            return Index == Axis ? (bPositive ? 2 : -2) : -1;
        };

        auto GetEndIndexComponent = [Axis, bPositive](int8 Index)
        {
            return Index == Axis ? (bPositive ? 3 : -1) : 2;
        };

        FCubeGrid::ForEach(
            {
                GetStartIndexComponent(0),
                GetStartIndexComponent(1),
                GetStartIndexComponent(2),
                GetStartIndexComponent(3)
            },
            {
                GetEndIndexComponent(0),
                GetEndIndexComponent(1),
                GetEndIndexComponent(2),
                GetEndIndexComponent(3)
            }, [this, InstancedStaticMeshComponent, SideIndex](const FIntVector4& Cell)
            {
                Grid.At(Cell) = new FSubcube(InstancedStaticMeshComponent, Cell, Colors[SideIndex]);
            });
    }

#if WITH_EDITOR
    if (bInGame)
    {
#endif
        Grid.ForEach([](FSubcube* Subcube)
            {
                Subcube->Reset();
                Subcube->Update();
            });
#if WITH_EDITOR
    }
#endif

    Super::PostRegisterAllComponents();
}

void ACubeActor::PreloadSounds()
{
    for (USoundWave* Sound : ClickSounds)
        FUtils::PreloadSound(this, Sound);
}

ACubeActor::~ACubeActor()
{
    Grid.ForEach([](FSubcube* Subcube)
        {
            delete Subcube;
        });
}

void ACubeActor::PlaySound() const
{
    FUtils::PlaySound(this, ClickSounds[FMath::RandRange(0, ClickSounds.Num() - 1)]);
}

void ACubeActor::ResetGrid()
{
    FCubeGrid Copy = Grid;
    Copy.ForEach([this](FSubcube* Subcube)
        {
            Subcube->Reset();
            Grid.At(Subcube->GetInitialCell()) = Subcube;
        });
}

void ACubeActor::UpdateSubcubes()
{
    Grid.ForEach([](FSubcube* Subcube)
        {
            Subcube->Update();
        });
}

const FCubeGrid* ACubeActor::GetGrid() const
{
    return &Grid;
}

FCubeGrid* ACubeActor::GetGrid()
{
    return &Grid;
}
