#include "Object4d.h"
#include "Camera.h"

FObject4d::~FObject4d()
{

}

void FObject4d::Rotate(const FMatrix5& Rotation)
{
    Transform = Rotation * TransformBeforeGrab;
}

void FObject4d::Update()
{
    Matrix = FCamera::GetProjection() * Transform;

    if (!UpdateInternal())
        return;

    for (int8 Index = 0; Index < 3; Index++)
        SetRow4(Index);

    FVector3f E0 = Matrix.GetColumn3(0);
    FVector3f E1 = Matrix.GetColumn3(1);
    FVector3f E2 = Matrix.GetColumn3(2);

    const FVector3f N0 = E1.Cross(E2);
    const FVector3f N1 = E2.Cross(E0);
    const FVector3f N2 = E0.Cross(E1);

    for (int8 Index = 0; Index < 3; Index++)
        SetRow3(Index, N0[Index], N1[Index], N2[Index]);
}

void FObject4d::SaveTransform()
{
    TransformBeforeGrab = Transform;
}

const FMatrix5& FObject4d::GetMatrix() const
{
    return Matrix;
}

const FMatrix5& FObject4d::GetTransform() const
{
    return Transform;
}

void FObject4d::SetTransform(const FMatrix5& TransformIn)
{
    Transform = TransformIn;
}

FObject4d::FObject4d()
{

}
