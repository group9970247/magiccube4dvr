#include "Compass.h"
#include "HandInterface.h"
#include "CubeActor.h"
#include "Utils.h"
#include "CubeSettings.h"
#include "Camera.h"

#include "UObject/ConstructorHelpers.h"
#include "HeadMountedDisplayFunctionLibrary.h"


UCompass::FBone::FBone(const FName& NameIn) :
    Name(NameIn)
{

}

void UCompass::FBone::SetTarget(float TargetIn, bool bSnapIn)
{
    Target = TargetIn;
    bSnap = bSnapIn;
}


UCompass::UCompass() :
    FComponent4d(Cast<UMeshComponent>(this))
{
    struct FConstructorStatics
    {
        ConstructorHelpers::FObjectFinder<UMaterial> Compass4dMaterial;
        ConstructorHelpers::FObjectFinder<UMaterial> CompassMaterial;

        ConstructorHelpers::FObjectFinder<USkeletalMesh> CompassLeft;
        ConstructorHelpers::FObjectFinder<USkeletalMesh> CompassRight;


        FConstructorStatics() :
            Compass4dMaterial(TEXT("/Game/Compass4dMaterial")),
            CompassMaterial(TEXT("/Game/CompassMaterial")),

            CompassLeft(TEXT("/Game/CompassLeft")),
            CompassRight(TEXT("/Game/CompassRight"))
        {

        }
    };
    static FConstructorStatics ConstructorStatics;

    SetMaterials(ConstructorStatics.Compass4dMaterial.Object, ConstructorStatics.CompassMaterial.Object);

    CompassLeft = ConstructorStatics.CompassLeft.Object;
    CompassRight = ConstructorStatics.CompassRight.Object;

    FMatrix44f& D = DeltaRotation;
                     D.M[1][0] = 0.f;                  D.M[3][0] = 0.f;
    D.M[0][1] = 0.f; D.M[1][1] = 1.f; D.M[2][1] = 0.f; D.M[3][1] = 0.f;
                     D.M[1][2] = 0.f;                  D.M[3][2] = 0.f;
    D.M[0][3] = 0.f; D.M[1][3] = 0.f; D.M[2][3] = 0.f; D.M[3][3] = 1.f;
}

void UCompass::SetHandInterface(const FHandInterface* HandInterfaceIn)
{
    HandInterface = HandInterfaceIn;
}

FBoxSphereBounds UCompass::CalcBounds(const FTransform& LocalToWorld) const
{
    if (HandInterface->IsHolding())
        return CalcBounds4d(FUtils::GetCubeActor());

    return Super::CalcBounds(LocalToWorld);
}

void UCompass::TickCompass(float DeltaTime, bool bCompassIsActive)
{
    bool bCompassOn = GetMutableDefault<UCubeSettings>()->bCompass;
    SetHiddenInGame(!bCompassOn);
    if (!bCompassOn)
        return;

    if (bCompassIsActive)
    {
        FRotator HeadRotationTrackingD;
        FVector HeadPositionTrackingD;
        UHeadMountedDisplayFunctionLibrary::GetOrientationAndPosition(HeadRotationTrackingD, HeadPositionTrackingD);
        FRotator3f HeadRotationTracking(HeadRotationTrackingD);
        FVector3f HeadPositionTracking(HeadPositionTrackingD);

        FTransform3f TrackingToWorld(UHeadMountedDisplayFunctionLibrary::GetTrackingToWorldTransform(this));
        FRotationTranslationMatrix44f HeadMatrixTracking(HeadRotationTracking, HeadPositionTracking);
        FMatrix44f HeadMatrixWorld = HeadMatrixTracking * TrackingToWorld.ToMatrixWithScale();
        FMatrix44f WorldToCompass(GetComponentTransform().ToInverseMatrixWithScale());
        FMatrix44f HeadMatrixCompass = HeadMatrixWorld * WorldToCompass;

        FVector3f CompassToHead(HeadMatrixCompass.GetOrigin());
        CompassToHead.X *= -1.f;
        CompassToHead.Normalize();
        FVector2f CompassToHead2(CompassToHead);
        float CompassToHead2Length = CompassToHead2.Size();
        CompassToHead2 /= CompassToHead2Length;

        Platform0.SetTarget(FUtils::GetAngle(CompassToHead2.X, CompassToHead2.Y), false);

        float Platform1Angle = FUtils::GetAngle(CompassToHead2Length, CompassToHead.Z);
        if (Platform1Angle <= PI)
        {
            Platform1.SetTarget(-Platform1Angle, false);
            Frame.SetTarget(HALF_PI, false);
        }
        else
        {
            Platform1.SetTarget(0.f, true);
            Platform1.bSnapStartAngleLessThanTarget = true;

            Frame.SetTarget(HALF_PI + TWO_PI - Platform1Angle, false);
        }

        FVector3f HeadUp = HeadMatrixCompass.GetScaledAxis(EAxis::Z);
        HeadUp.X *= -1.f;
        FVector3f HeadUpOrtho;
        Ortho(HeadUp, CompassToHead, &HeadUpOrtho);
        FVector3f B0123r0Right = FVector3f(-CompassToHead.Y, CompassToHead.X, 0.f).GetSafeNormal();
        FVector3f B0123r0Up = FVector3f::CrossProduct(CompassToHead, B0123r0Right);
        float CosB0123r0Target = FVector3f::DotProduct(HeadUpOrtho, B0123r0Up);
        float SinB0123r0Target = FVector3f::DotProduct(HeadUpOrtho, -B0123r0Right);

        B0123r0.SetTarget(FUtils::GetAngle(CosB0123r0Target, SinB0123r0Target), false);

        float Angle23 = HALF_PI - FCamera::GetAngles()[(int32)ECameraRotationPlane::Plane23];

        B0123r1.SetTarget(Angle23, false);

        const FMatrix44f& Rotation = FCamera::GetRotation012();
        FMatrix44f Adjust23 = FRotationMatrix44f::MakeFromXY({1.f, 0.f, 0.f}, {0.f, FMath::Cos(-Angle23), FMath::Sin(-Angle23)});

        FVector3f E0 = Adjust23.TransformVector(Rotation.GetScaledAxis(EAxis::X));
        E0.X *= -1.f;

        FVector3f E1 = Adjust23.TransformVector(Rotation.GetScaledAxis(EAxis::Y));
        E1.X *= -1.f;

        FVector3f E1Ortho = FVector3f(E1.X, 0.f, E1.Z).GetSafeNormal();

        B012r2.SetTarget(FUtils::GetAngle(E1Ortho.X, -E1Ortho.Z), false);
        B012r0.SetTarget(FUtils::GetAngle(E1, E1Ortho, {0.f, 1.f, 0.f}), false);

        FVector3f E0Current = {E1Ortho.Z, 0.f, -E1Ortho.X};

        B012r1.SetTarget(FUtils::GetAngle(E0, E0Current, FVector3f::CrossProduct(E1, E0Current)), false);
        B0.SetTarget(-HALF_PI, false);
    }
    else
    {
        Platform0.SetTarget(0.f, true);
        Platform1.SetTarget(0.f, true);
        Frame.SetTarget(0.f, true);
        B0123r0.SetTarget(0.f, true);
        B0123r1.SetTarget(0.f, true);
        B012r2.SetTarget(0.f, true);
        B012r0.SetTarget(0.f, true);
        B012r1.SetTarget(0.f, true);
        B0.SetTarget(0.f, true);
    }

    TickBone(&Platform0, DeltaTime);
    TickBone(&Platform1, DeltaTime);
    TickBone(&Frame, DeltaTime);
    TickBone(&B0123r0, DeltaTime);
    TickBone(&B0123r1, DeltaTime);
    TickBone(&B012r2, DeltaTime);
    TickBone(&B012r0, DeltaTime);
    TickBone(&B012r1, DeltaTime);
    TickBone(&B0, DeltaTime);

    MarkRefreshTransformDirty();
    bCompassWasActive = bCompassIsActive;
}

void UCompass::SetHand(EControllerHand Hand, const FMatrix44f& CubeToCompass)
{
    SetSkinnedAssetAndUpdate(Hand == EControllerHand::Left ? CompassLeft : CompassRight);
    InstancePreventGC = CreateObject4dInstance();

    const FMatrix44f& C = CubeToCompass;
    SetCubeToThis({
        {C.M[0][0], C.M[1][0], C.M[2][0], 0.f, C.M[3][0]},
        {C.M[0][1], C.M[1][1], C.M[2][1], 0.f, C.M[3][1]},
        {C.M[0][2], C.M[1][2], C.M[2][2], 0.f, C.M[3][2]},
        {0.f,       0.f,       0.f,       1.f, 0.f},
        {0.f,       0.f,       0.f,       0.f, 1.f}
    });
}

void UCompass::GrabStart(const FMatrix5& ToGrabbedCube)
{
    if (GetMutableDefault<UCubeSettings>()->bCompass)
        EnableObject4dInstance(ToGrabbedCube);
}

void UCompass::RotateHandStart()
{
    if (GetMutableDefault<UCubeSettings>()->bCompass)
        SaveTransform();
}

void UCompass::RotateHand(const FMatrix5& SideRotation)
{
    if (!GetMutableDefault<UCubeSettings>()->bCompass)
        return;

    SetWorldTransform(FUtils::GetCubeTransform());
    Rotate(SideRotation);
    Update();
}

void UCompass::ReleaseHand()
{
    if (GetMutableDefault<UCubeSettings>()->bCompass)
        DisableObject4dInstance();
}

double UCompass::GetBoundsRadius(float Matrix44) const
{
    return 100.f;
}

void UCompass::TickBone(FBone* Bone, float DeltaTime)
{
    int32 Index = GetBoneIndex(Bone->Name);
    if (Index < 0 || Index >= BoneSpaceTransforms.Num())
        return;

    FTransform& BoneTransform = BoneSpaceTransforms[Index];

    float CurrentAngle = -BoneTransform.GetRotation().GetAngle() * BoneTransform.GetRotation().GetRotationAxis().Y;
    float DeltaAngleTotal = Bone->Target - CurrentAngle;
    if (FMath::Abs(DeltaAngleTotal) > PI)
        CurrentAngle += FMath::Sign(DeltaAngleTotal) * TWO_PI;

    float Acceleration = (Bone->Target - CurrentAngle) * 100.f - Bone->Speed * 8.f;
    Bone->Speed += Acceleration * DeltaTime;
    float DeltaAngle = Bone->Speed * DeltaTime;

    if (Bone->bSnap)
    {
        if (bCompassWasActive)
            Bone->bSnapStartAngleLessThanTarget = CurrentAngle < Bone->Target;

        if ((CurrentAngle + DeltaAngle < Bone->Target) != Bone->bSnapStartAngleLessThanTarget)
        {
            DeltaAngle = Bone->Target - CurrentAngle;
            Bone->Speed = 0.f;
        }
    }

    float CosF, SinF;
    FMath::SinCos(&SinF, &CosF, DeltaAngle);
    SinF *= -1.f;


    FMatrix44f& D = DeltaRotation;
    D.M[0][0] = CosF;                   D.M[2][0] = SinF;

    D.M[0][2] = -SinF;                  D.M[2][2] = CosF;


    BoneTransform.SetFromMatrix(FMatrix44d(DeltaRotation * FMatrix44f(BoneTransform.ToMatrixNoScale())));
}

void UCompass::Ortho(const FVector3f& Vector, const FVector3f& Axis, FVector3f* Result) const
{
    *Result = (Vector - FVector3f::DotProduct(Vector, Axis) * Axis).GetSafeNormal();
}
