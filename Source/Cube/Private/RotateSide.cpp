#include "RotateSide.h"
#include "Utils.h"
#include "CubeGrid.h"
#include "Subcube.h"

int8 FRotateSide::RotationAxis0;
int8 FRotateSide::RotationAxis1;
FIntVector4 FRotateSide::RotatingSideStart;
FIntVector4 FRotateSide::RotatingSideEnd;
FMatrix5 FRotateSide::SideRotation;

void FRotateSide::Start(int8 SideAxisIndex, bool bSideAxisPositive, int8 RotationAxis0In, int8 RotationAxis1In)
{
    RotatingSideStart = FIntVector4(-2);
    RotatingSideEnd = FIntVector4(3);
    if (bSideAxisPositive)
        RotatingSideStart[SideAxisIndex] = 1;
    else
        RotatingSideEnd[SideAxisIndex] = 0;

    RotationAxis0 = RotationAxis0In;
    RotationAxis1 = RotationAxis1In;

    SideRotation = FMatrix5::Identity;
}

void FRotateSide::Rotate(float CosF, float SinF)
{
    FMatrix5& R = SideRotation;
    R[RotationAxis0][RotationAxis0] = CosF; R[RotationAxis0][RotationAxis1] = -SinF;
    R[RotationAxis1][RotationAxis0] = SinF; R[RotationAxis1][RotationAxis1] = CosF;

    FUtils::GetGrid()->ForEach(RotatingSideStart, RotatingSideEnd, [](FSubcube* Subcube)
        {
            Subcube->Rotate(SideRotation);
        });
}

void FRotateSide::Update()
{
    FUtils::GetGrid()->ForEach(RotatingSideStart, RotatingSideEnd, [](FSubcube* Subcube)
        {
            Subcube->Update();
        });
}

void FRotateSide::End(int8 EndAngleI)
{
    float CosF, SinF;
    FUtils::CosAndSinFromQuarterIndex(EndAngleI, &CosF, &SinF);

    Rotate(CosF, SinF);

    if (EndAngleI == 0)
        return;

    FCubeGrid* Grid = FUtils::GetGrid();
    FCubeGrid Copy = *Grid;
    FCubeGrid::ForEach(RotatingSideStart, RotatingSideEnd, [CosF, SinF, &Copy, Grid](const FIntVector4& Cell)
        {
            FIntVector4 RotatedCell = Cell;
            RotatedCell[RotationAxis0] = CosF * Cell[RotationAxis0] - SinF * Cell[RotationAxis1];
            RotatedCell[RotationAxis1] = SinF * Cell[RotationAxis0] + CosF * Cell[RotationAxis1];

            FSubcube* Subcube = Copy.At(Cell);
            Grid->At(RotatedCell) = Subcube;

            if (Subcube)
                Subcube->SaveTransform();
        });
}

const FMatrix5& FRotateSide::GetSideRotation()
{
    return SideRotation;
}

float FRotateSide::GetAngle()
{
    return FUtils::GetAngle(SideRotation[RotationAxis0][RotationAxis0], SideRotation[RotationAxis1][RotationAxis0]);
}
