#pragma once


#include "CubeGrid.h"

#include "CoreMinimal.h"
#include "HAL/Runnable.h"
#include "Containers/List.h"


class FSteam : public FRunnable
{
public:
    static void Initialize();
    static void Deinitialize();

    static void CommitGridState();


    virtual uint32 Run() override;

private:
    struct FAchievement
    {
        const char* Id;
        bool bAchieved = false;
        TArray<TArray<FIntVector4>> Or;
    };


    static FSteam* Instance;
    static FRunnableThread* Thread;
    static TArray<FAchievement> Achievements;
    static TAtomic<bool> bStop;
    static FCriticalSection GridStatesCriticalSection;
    static TDoubleLinkedList<FCubeGrid> GridStates;


    FSteam();

    void Add3Achievements(TFunction<void(int8 MinZeroCount)> Add);
    void AddAchievement(int8 Height, int8 MinZeroCount);
    void AddOr(int8 SideAxis, bool bPositive, int8 Height, int8 MinZeroCount);
};
