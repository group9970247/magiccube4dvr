#pragma once

#include "GameFramework/Pawn.h"
#include "CubePawn.generated.h"


class FHandInterface;
enum class ECameraRotationPlane : int32;

struct FInputActionInstance;
class UInputAction;


UCLASS()
class ACubePawn : public APawn
{
    GENERATED_BODY()

public:
    const float DefaultHeight = 150.f;


    ACubePawn();

    const FHandInterface* GetCameraHand() const;
    void ResetCameraHand();

    virtual void Tick(float DeltaSeconds) override;
    virtual void BeginPlay() override;
    virtual void PostRegisterAllComponents() override;

    void FirstTick();
    void MenuStarted();

protected:
    virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

private:
    FHandInterface* LeftHand = nullptr;
    FHandInterface* RightHand = nullptr;

    bool bHandTracking = true;
    const FHandInterface* CameraHand;

    bool bOculusThumbstickLeftPressed = false;
    bool bOculusThumbstickRightPressed = false;

    const UInputAction* FrameCube;

    const UInputAction* Rotate012Left;
    const UInputAction* Rotate23Left;

    const UInputAction* Rotate012Right;
    const UInputAction* Rotate23Right;

    const UInputAction* GrabLeft;
    const UInputAction* GrabRight;

    const UInputAction* MenuLeft;
    const UInputAction* MenuRight;

    const UInputAction* Menu;
    const UInputAction* Press;

    const UInputAction* OculusThumbstickButtonLeft;
    const UInputAction* OculusRotate012Left;
    const UInputAction* OculusRotate23Left;

    const UInputAction* OculusThumbstickButtonRight;
    const UInputAction* OculusRotate012Right;
    const UInputAction* OculusRotate23Right;


    void FrameCubeStarted();

    void Rotate012LeftTriggered(const FInputActionInstance& Instance);
    void Rotate012LeftCompleted(const FInputActionInstance& Instance);

    void Rotate23LeftTriggered(const FInputActionInstance& Instance);
    void Rotate23LeftCompleted(const FInputActionInstance& Instance);

    void Rotate012RightTriggered(const FInputActionInstance& Instance);
    void Rotate012RightCompleted(const FInputActionInstance& Instance);

    void Rotate23RightTriggered(const FInputActionInstance& Instance);
    void Rotate23RightCompleted(const FInputActionInstance& Instance);

    void GrabLeftStarted();
    void GrabLeftCompleted();

    void GrabRightStarted();
    void GrabRightCompleted();

    void MenuLeftStarted(const FInputActionInstance& Instance);
    void MenuRightStarted(const FInputActionInstance& Instance);

    void PressStarted();

    void OculusThumbstickButtonLeftStarted(const FInputActionInstance& Instance);
    void OculusThumbstickButtonLeftCompleted(const FInputActionInstance& Instance);

    void OculusRotate012LeftTriggered(const FInputActionInstance& Instance);
    void OculusRotate23LeftTriggered(const FInputActionInstance& Instance);

    void OculusThumbstickButtonRightStarted(const FInputActionInstance& Instance);
    void OculusThumbstickButtonRightCompleted(const FInputActionInstance& Instance);

    void OculusRotate012RightTriggered(const FInputActionInstance& Instance);
    void OculusRotate23RightTriggered(const FInputActionInstance& Instance);

    void Rotate012Triggered(const FHandInterface* Hand, const FInputActionInstance& Instance);
    void Rotate23Triggered(const FHandInterface* Hand, const FInputActionInstance& Instance);

    void Grab(FHandInterface* Hand);
    void Release(FHandInterface* Hand);

    void MenuStep(const FHandInterface* Hand, const FInputActionInstance& Instance);

    void RotateCamera(const FHandInterface* Hand, ECameraRotationPlane CameraRotationPlane, float Value);
    bool CellIsInRotatingPart(const FIntVector4& Cell, const FIntVector4& RotatingCell) const;

    template <typename T>
    void CreateHands();

    void StopCameraRotation();

    void GetFramedCubePosition(FVector3f* Position) const;
    void FrameCubeToPosition(const FVector3f& Position);
};
