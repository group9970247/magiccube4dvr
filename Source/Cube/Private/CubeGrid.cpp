#include "CubeGrid.h"

void FCubeGrid::ForEach(TFunction<void(const FIntVector4& Cell)> Do)
{
    ForEach({-2, -2, -2, -2}, {3, 3, 3, 3}, Do);
}

void FCubeGrid::ForEach(const FIntVector4& Start, const FIntVector4& End, TFunction<void(const FIntVector4& Cell)> Do)
{
    for (FIntVector4 Cell = Start; Cell[3] < End[3]; Cell[3]++)
    {
        for (Cell[2] = Start[2]; Cell[2] < End[2]; Cell[2]++)
        {
            for (Cell[1] = Start[1]; Cell[1] < End[1]; Cell[1]++)
            {
                for (Cell[0] = Start[0]; Cell[0] < End[0]; Cell[0]++)
                    Do(Cell);
            }
        }
    }
}

FCubeGrid::FCubeGrid()
{
    ForEach([this](const FIntVector4& Cell)
        {
            At(Cell) = nullptr;
        });
}

const FSubcube* FCubeGrid::GetSideCenter(int8 Axis, bool bPositive) const
{
    auto GetIndexComponent = [Axis, bPositive](int8 Index)
    {
        return Index == Axis ? (bPositive ? 2 : -2) : 0;
    };
    return At({
        GetIndexComponent(0),
        GetIndexComponent(1),
        GetIndexComponent(2),
        GetIndexComponent(3)});
}

FSubcube* const& FCubeGrid::At(const FIntVector4& Index) const
{
    return Cells[Index[3] + 2][Index[2] + 2][Index[1] + 2][Index[0] + 2];
}

FSubcube*& FCubeGrid::At(const FIntVector4& Index)
{
    return const_cast<FSubcube*&>(const_cast<const FCubeGrid*>(this)->At(Index));
}

void FCubeGrid::ForEach(TFunction<void(FSubcube* Subcube)> Do)
{
    ForEach({-2, -2, -2, -2}, {3, 3, 3, 3}, Do);
}

void FCubeGrid::ForEach(TFunction<void(const FSubcube* Subcube)> Do) const
{
    ForEach({-2, -2, -2, -2}, {3, 3, 3, 3}, Do);
}

void FCubeGrid::ForEach(const FIntVector4& Start, const FIntVector4& End, TFunction<void(const FSubcube* Subcube)> Do) const
{
    const_cast<FCubeGrid*>(this)->ForEach(Start, End, [Do](FSubcube* Subcube)
        {
            Do(const_cast<const FSubcube*>(Subcube));
        });
}

void FCubeGrid::ForEach(const FIntVector4& Start, const FIntVector4& End, TFunction<void(FSubcube* Subcube)> Do)
{
    ForEach(Start, End, [this, Do](const FIntVector4& Cell)
        {
            if (FSubcube* Subcube = At(Cell))
                Do(Subcube);
        });
}
