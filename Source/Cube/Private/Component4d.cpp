#include "Component4d.h"
#include "Components/PrimitiveComponent.h"

UPrimitiveComponent* FComponent4d::GetComponent() const
{
    return Component;
}

FComponent4d::FComponent4d()
{

}

FComponent4d::FComponent4d(UPrimitiveComponent* ComponentIn) :
    Component(ComponentIn)
{

}

void FComponent4d::SetMaterials(UMaterial* Object4dMaterialIn, UMaterial* Object3dMaterialIn)
{
    Object4dMaterial = Object4dMaterialIn;
    Object3dMaterial = Object3dMaterialIn;
}

UMaterialInstanceDynamic* FComponent4d::CreateObject4dInstance()
{
    Object4dInstance = UMaterialInstanceDynamic::Create(Object4dMaterial, Component);
    return Object4dInstance;
}

void FComponent4d::SetCubeToThis(const FMatrix5& CubeToThisIn)
{
    CubeToThis = CubeToThisIn;
}

void FComponent4d::ForceUpdateBounds()
{
    Component->UpdateBounds();
    Component->MarkRenderStateDirty();
}

FBoxSphereBounds FComponent4d::CalcBounds4d(const AActor* Cube) const
{
    const FTransform& ActorTransform = Cube->GetActorTransform();
    return {ActorTransform.TransformPosition(BoundsOriginRelative), BoundsBoxExtent, BoundsSphereRadius};
}

void FComponent4d::EnableObject4dInstance(const FMatrix5& ToGrabbedCube)
{
    Component->SetMaterial(0, Object4dInstance);
    SetTransform(ToGrabbedCube * CubeToThis);
    Update();
}

void FComponent4d::DisableObject4dInstance()
{
    Component->SetMaterial(0, Object3dMaterial);
    ForceUpdateBounds();
}

bool FComponent4d::UpdateInternal()
{
    const FMatrix5& MatrixLocal = GetMatrix();
    BoundsOriginRelative = FVector(MatrixLocal.GetColumn3(4));
    BoundsSphereRadius = GetBoundsRadius(MatrixLocal[4][4]);
    BoundsBoxExtent = FVector(BoundsSphereRadius);
    ForceUpdateBounds();
    return true;
}

void FComponent4d::SetRow4(int8 RowIndex)
{
    static const TArray<FName> Row4Names = {
        "PositionRow0",
        "PositionRow1",
        "PositionRow2"
    };

    const FVector5& Row = GetMatrix()[RowIndex];
    FVector4 Row4(Row[0], Row[1], Row[2], Row[4]);
    Object4dInstance->SetVectorParameterValue(Row4Names[RowIndex], Row4);
}

void FComponent4d::SetRow3(int8 RowIndex, float R0, float R1, float R2)
{
    static const TArray<FName> Row3Names = {
        "NormalRow0",
        "NormalRow1",
        "NormalRow2"
    };

    Object4dInstance->SetVectorParameterValue(Row3Names[RowIndex], FVector(R0, R1, R2));
}
