#include "MenuActor.h"
#include "MenuWidget.h"
#include "Utils.h"
#include "CubeActor.h"
#include "CubePawn.h"

#include "UObject/ConstructorHelpers.h"
#include "Components/WidgetComponent.h"


AMenuActor::AMenuActor()
{
    struct FConstructorStatics
    {
        ConstructorHelpers::FClassFinder<UMenuWidget> Menu;

        ConstructorHelpers::FObjectFinder<USoundWave> Select;
        ConstructorHelpers::FObjectFinder<USoundWave> Press;


        FConstructorStatics() :
            Menu(TEXT("/Game/Menu")),

            Select(TEXT("/Game/Select")),
            Press(TEXT("/Game/Press"))
        {

        }
    };
    static FConstructorStatics ConstructorStatics;


    RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");

    UWidgetComponent* WidgetComponent = CreateDefaultSubobject<UWidgetComponent>("Widget");
    WidgetComponent->SetGeometryMode(EWidgetGeometryMode::Cylinder);
    WidgetComponent->SetCylinderArcAngle(120.f);
    WidgetComponent->SetWidgetClass(ConstructorStatics.Menu.Class);
    WidgetComponent->SetDrawSize({2150.0, 700.0});
    WidgetComponent->SetRelativeLocation({-90.0, 0.0, 0.0});
    WidgetComponent->SetRelativeScale3D(FVector(0.2));
    WidgetComponent->SetupAttachment(RootComponent);

    SelectSound = ConstructorStatics.Select.Object;
    PressSound = ConstructorStatics.Press.Object;
}

void AMenuActor::Initialize()
{
    PreloadSounds();
    GetMenu()->InitializeIntensity();
}

UMenuWidget* AMenuActor::GetMenu()
{
    return Cast<UMenuWidget>(Cast<UWidgetComponent>(RootComponent->GetAttachChildren()[0])->GetWidget());
}

void AMenuActor::SwitchMenu()
{
    UMenuWidget* MenuWidget = GetMenu();
    bool bInMenu = FUtils::InMenu();

    if (bInMenu)
    {
        if (!MenuWidget->NewGameWasStarted())
            return;
    }
    else
    {
        FMatrix44f HeadMatrixWorld;
        FUtils::GetHeadMatrixWorld(&HeadMatrixWorld);
        FVector3f HeadLocation = HeadMatrixWorld.GetOrigin();
        FVector2f CubeLocation = FVector2f(FVector2d(FUtils::GetCubeActor()->GetActorLocation()));
        FVector3f HeadToCube = FVector3f((CubeLocation - FVector2f(HeadLocation)).GetSafeNormal(), 0.f);
        float Angle = FUtils::GetAngle({HeadToCube.Y, -HeadToCube.X, 0.f}, {1.f, 0.f, 0.f}, {0.f, 1.f, 0.f});
        SetActorLocation({HeadLocation.X, HeadLocation.Y, FUtils::GetPawn()->DefaultHeight});
        SetActorRotation(FRotator(0.0, Angle / PI * 180.0, 0.0));
        MenuWidget->SelectDefault();
    }

    SetActorHiddenInGame(bInMenu);
}

void AMenuActor::Select() const
{
    FUtils::PlaySound(this, SelectSound);
}

void AMenuActor::Press() const
{
    FUtils::PlaySound(this, PressSound);
}

void AMenuActor::PreloadSounds()
{
    FUtils::PreloadSound(this, SelectSound);
    FUtils::PreloadSound(this, PressSound);
}
