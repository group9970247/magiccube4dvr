#pragma once
#include "CoreMinimal.h"

class FSubcube;

class FCubeGrid
{
public:
    static void ForEach(TFunction<void(const FIntVector4& Cell)> Do);
    static void ForEach(const FIntVector4& Start, const FIntVector4& End, TFunction<void(const FIntVector4& Cell)> Do);


    FCubeGrid();
    const FSubcube* GetSideCenter(int8 Axis, bool bPositive) const;

    FSubcube* const& At(const FIntVector4& Index) const;
    FSubcube*& At(const FIntVector4& Index);

    void ForEach(TFunction<void(FSubcube* Subcube)> Do);
    void ForEach(TFunction<void(const FSubcube* Subcube)> Do) const;
    void ForEach(const FIntVector4& Start, const FIntVector4& End, TFunction<void(const FSubcube* Subcube)> Do) const;
    void ForEach(const FIntVector4& Start, const FIntVector4& End, TFunction<void(FSubcube* Subcube)> Do);

private:
    FSubcube* Cells[5][5][5][5];
};
