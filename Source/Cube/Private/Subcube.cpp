#include "Subcube.h"
#include "CubeActor.h"

#include "Components/InstancedStaticMeshComponent.h"


FSubcube::FSubcube(UInstancedStaticMeshComponent* InstancedStaticMeshComponentIn, const FIntVector4& InitialCellIn, const FLinearColor& Color) :
    Component(InstancedStaticMeshComponentIn),
    InitialCell(InitialCellIn),
    CullDistance(Component->InstanceEndCullDistance * 2),
    HiddenTransform(FVector(CullDistance, 0.0, 0.f))
{
    InstanceIndex = Component->AddInstance(HiddenTransform);

    Component->SetCustomDataValue(InstanceIndex, 0, Color.R, true);
    Component->SetCustomDataValue(InstanceIndex, 1, Color.G, true);
    Component->SetCustomDataValue(InstanceIndex, 2, Color.B, true);
}

const FIntVector4& FSubcube::GetInitialCell() const
{
    return InitialCell;
}

void FSubcube::Reset()
{
    int32 SideIndex = -1;
    for (int32 Index = 0; Index < 4; Index++)
    {
        if (FMath::Abs(InitialCell[Index]) == 2)
        {
            SideIndex = Index;
            break;
        }
    }
    bool bSidePositive = InitialCell[SideIndex] > 0;

    TArray<FVector5> E;
    TArray<FVector5> EI;

    const FVector5 E0 = FMatrix5::Identity.GetColumn(0);
    const FVector5 E1 = FMatrix5::Identity.GetColumn(1);
    const FVector5 E2 = FMatrix5::Identity.GetColumn(2);
    const FVector5 E3 = FMatrix5::Identity.GetColumn(3);

    if (SideIndex == 0)
    {
        if (bSidePositive)
        {
            E = {-E3, E1, E2, E0};
            EI = {E3, E1, E2, -E0};
        }
        else
        {
            E = {E3, E1, E2, -E0};
            EI = {-E3, E1, E2, E0};;
        }
    }
    if (SideIndex == 1)
    {
        if (bSidePositive)
        {
            E = {E0, -E3, E2, E1};
            EI = {E0, E3, E2, -E1};
        }
        else
        {
            E = {E0, E3, E2, -E1};
            EI = {E0, -E3, E2, E1};
        }
    }
    if (SideIndex == 2)
    {
        if (bSidePositive)
        {
            E = {E0, E1, -E3, E2};
            EI = {E0, E1, E3, -E2};
        }
        else
        {
            E = {E0, E1, E3, -E2};
            EI = {E0, E1, -E3, E2};
        }
    }
    if (SideIndex == 3)
    {
        if (bSidePositive)
        {
            E = {E0, E1, E2, E3};
            EI = {E0, E1, E2, E3};
        }
        else
        {
            E = {E0, E1, -E2, -E3};
            EI = {E0, E1, -E2, -E3};
        }
    }

    const FVector5 Unit4 = {0.f, 0.f, 0.f, 0.f, 1.f};

    FMatrix5 Side0001ToThisSide = FMatrix5::FromColumns(E[0], E[1], E[2], E[3], Unit4);
    FMatrix5 ThisSideToSide0001 = FMatrix5::FromColumns(EI[0], EI[1], EI[2], EI[3], Unit4);

    FVector5 GridLocation = {float(InitialCell[0]), float(InitialCell[1]), float(InitialCell[2]), float(InitialCell[3]), 0.f};
    FVector5 GridLocation0001 = ThisSideToSide0001 * GridLocation;
    FIntVector4 InitialCell0001 = {
        FMath::RoundToInt(GridLocation0001[0]),
        FMath::RoundToInt(GridLocation0001[1]),
        FMath::RoundToInt(GridLocation0001[2]),
        FMath::RoundToInt(GridLocation0001[3])};
    float GridToWorld3ScaleFactor = ACubeActor::SubcubeSideLength + ACubeActor::MarginLength;
    FVector5 Location0001 = {
        InitialCell0001[0] * GridToWorld3ScaleFactor,
        InitialCell0001[1] * GridToWorld3ScaleFactor,
        InitialCell0001[2] * GridToWorld3ScaleFactor,
        ACubeActor::CubeSideLength / 2.f,
        1.f};
    FMatrix5 Transform0001 = FMatrix5::FromColumns(E0, E1, E2, E3, Location0001);
    SetTransform(Side0001ToThisSide * Transform0001);

    SaveTransform();
}

bool FSubcube::IsVisible() const
{
    return GetInstanceMatrix().M[3][0] < CullDistance - 1.f;
}

bool FSubcube::UpdateInternal()
{
    const FMatrix5& MatrixLocal = GetMatrix();
    float MinDepth = 0.05f;
    bool bShouldBeVisible = MatrixLocal[3][3] > MinDepth * MatrixLocal[4][4];
    Component->UpdateInstanceTransform(InstanceIndex, bShouldBeVisible ? FTransform(FVector(MatrixLocal[0][4], MatrixLocal[1][4], MatrixLocal[2][4])) : HiddenTransform, false, true, true);
    return bShouldBeVisible;
}

void FSubcube::SetRow4(int8 RowIndex)
{
    int32 CustomDataIndex = 3 + RowIndex * 3;
    const FVector5& Row = GetMatrix()[RowIndex];

    for (int8 Index = 0; Index < 3; Index++)
        Component->SetCustomDataValue(InstanceIndex, CustomDataIndex + Index, Row[Index], true);
}

void FSubcube::SetRow3(int8 RowIndex, float R0, float R1, float R2)
{
    int32 CustomDataIndex = 12 + RowIndex * 3;

    Component->SetCustomDataValue(InstanceIndex, CustomDataIndex + 0, R0, true);
    Component->SetCustomDataValue(InstanceIndex, CustomDataIndex + 1, R1, true);
    Component->SetCustomDataValue(InstanceIndex, CustomDataIndex + 2, R2, true);
}

const FMatrix& FSubcube::GetInstanceMatrix() const
{
    return Component->PerInstanceSMData[InstanceIndex].Transform;
}
