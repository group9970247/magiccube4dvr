#pragma once

#include "CoreMinimal.h"
#include "Math/MathFwd.h"


class ACubeGameMode;
class ACubeActor;
class FCubeGrid;
class ACubePawn;
class AMenuActor;

class USoundWave;
class AActor;
class APlayerController;


class FUtils
{
public:
    static void Initialize(ACubeGameMode* GameModeIn);
    static void BeginPlay();
    static void UpdateContollerType(const struct FXRMotionControllerData& MotionControllerData);

    static float GetAngle(float CosP, float SinP);
    static float GetAngle(const FVector3f& Vector, const FVector3f& E0, const FVector3f& E1);
    static void CosAndSinFromQuarterIndex(int8 Index, float* CosT, float* SinT);
    static void GetHeadMatrixWorld(FMatrix44f* HeadMatrixWorld);
    static void GetOtherAxises(int8 Axis, TArray<int8>* OtherAxises);

    static ACubeGameMode* GetGameMode();
    static ACubeActor* GetCubeActor();
    static const FTransform& GetCubeTransform();
    static const FCubeGrid* GetConstGrid();
    static FCubeGrid* GetGrid();
    static ACubePawn* GetPawn();
    static APlayerController* GetPlayerController();
    static AMenuActor* GetMenuActor();
    static class UMenuWidget* GetMenuWidget();
    static bool InMenu();
    static bool ValveIndexController();

    static void PreloadSound(const AActor* Actor, USoundWave* Sound);
    static void PlaySound(const AActor* Actor, USoundWave* Sound);

    static void Quit();

private:
    static ACubeGameMode* GameMode;
    static ACubeActor* CubeActor;
    static ACubePawn* Pawn;
    static APlayerController* PlayerController;
    static AMenuActor* MenuActor;
    static FName DeviceName;
    static bool bValveIndexController;


    static void PlaySoundInternal(const AActor* Actor, USoundWave* Sound, float Volume);
};
