#pragma once

#include "MotionControllerComponent.h"
#include "HandInterface.h"
#include "CubeMotionControllerComponent.generated.h"

UCLASS()
class UCubeMotionControllerComponent : public UMotionControllerComponent, public FHandInterface
{
    GENERATED_BODY()

//public:
//    UCubeMotionControllerComponent();
    virtual void SetHand(EControllerHand HandIn) override;
//    virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

protected:
    virtual bool IsLeft() const override;
    virtual void GetGrabPoint(FVector& GrabPoint) const override;
    virtual void GetDownVector(FVector3f* Down) const override;
    virtual void GetForwardVector(FVector3f* Forward) const override;
    virtual void GrabStart(const FMatrix5& ToGrabbedCube) override;
    virtual void MoveHandStart(const FMatrix5& ToGrabbedCube) override;
    virtual void RotateHand(const FMatrix5& SideRotation) override;

    virtual double GetBoundsRadius(float Matrix44) const override;
};
