#include "RiggedHand.h"
#include "CubeGameMode.h"
#include "Compass.h"
#include "CubePawn.h"
#include "CubeActor.h"
#include "Utils.h"
#include "Camera.h"

#include "UObject/ConstructorHelpers.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Haptics/HapticFeedbackEffect_Curve.h"


URiggedHand::URiggedHand() :
    FHandInterface(
        Cast<UPrimitiveComponent>(this),
        0.f,
        (ACubeActor::MarginLength / 2.f + ACubeActor::SubcubeSideLength +
            ACubeActor::MarginLength / 2.f) / ACubeActor::CubeSideLength)
{
    struct FConstructorStatics
    {
        ConstructorHelpers::FObjectFinder<UMaterial> Hand4dMaterial;
        ConstructorHelpers::FObjectFinder<UMaterial> HandMaterial;

        ConstructorHelpers::FObjectFinder<UHapticFeedbackEffect_Curve> GrabCurve;
        ConstructorHelpers::FObjectFinder<UHapticFeedbackEffect_Curve> ReleaseCurve;


        FConstructorStatics() :
            Hand4dMaterial(TEXT("/Game/Hand4dMaterial")),
            HandMaterial(TEXT("/Game/HandMaterial")),

            GrabCurve(TEXT("/Game/GrabCurve")),
            ReleaseCurve(TEXT("/Game/ReleaseCurve"))
        {

        }
    };
    static FConstructorStatics ConstructorStatics;

    SetMaterials(ConstructorStatics.Hand4dMaterial.Object, ConstructorStatics.HandMaterial.Object);

    GrabCurve = ConstructorStatics.GrabCurve.Object;
    ReleaseCurve = ConstructorStatics.ReleaseCurve.Object;

    FMatrix& F = FixBoneRotation;
    F.M[0][0] = 0.0; F.M[1][0] = -1.0; F.M[2][0] = 0.0; F.M[3][0] = 0.0;
    F.M[0][1] = 1.0; F.M[1][1] = 0.0;  F.M[2][1] = 0.0; F.M[3][1] = 0.0;
    F.M[0][2] = 0.0; F.M[1][2] = 0.0;  F.M[2][2] = 1.0; F.M[3][2] = 0.0;
    F.M[0][3] = 0.0; F.M[1][3] = 0.0;  F.M[2][3] = 0.0; F.M[3][3] = 1.0;

    Compass = CreateDefaultSubobject<UCompass>("Compass");
    Compass->SetHandInterface(static_cast<FHandInterface*>(this));
}

void URiggedHand::OnRegister()
{
    Super::OnRegister();
    Compass->SetupAttachment(this);
}

void URiggedHand::SetHand(EControllerHand HandIn)
{
    FMatrix44f CubeToHand;
    SetHandInternal(HandIn, &CubeToHand);

    Compass->SetHand(GetHand(), HandToCompass * CubeToHand);
}

void URiggedHand::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    FXRMotionControllerData MotionControllerData;
    if (!GetControllerData(&MotionControllerData))
        return;

    FUtils::UpdateContollerType(MotionControllerData);
    TickHand(DeltaTime);

    bool bThisHand = FUtils::GetPawn()->GetCameraHand() == static_cast<const FHandInterface*>(this);
    Compass->TickCompass(DeltaTime, bThisHand && FCamera::GetCameraRotation() != ECameraRotationState::None);

    GetBoneTransform(MotionControllerData, EHandKeypoint::Wrist, WristTransform);

    if (IsHolding())
    {
        if (GrabState == EGrabState::Move)
            UpdateMoveTransform();
    }
    else
        UpdateReleaseTransform(MotionControllerData);
}

FBoxSphereBounds URiggedHand::CalcBounds(const FTransform& LocalToWorld) const
{
    if (IsHolding())
        return CalcBounds4d(FUtils::GetCubeActor());

    return Super::CalcBounds(LocalToWorld);
}

bool URiggedHand::IsLeft() const
{
    return GetHand() == EControllerHand::Left;
}

void URiggedHand::GetGrabPoint(FVector& GrabPoint) const
{
    GrabPoint = GetComponentTransform().TransformPosition(FVector(HandToCube.GetOrigin()));
}

void URiggedHand::GetDownVector(FVector3f* Down) const
{
    SubcubeToWorld({0.f, 0.f, -1.f}, Down);
}

void URiggedHand::GetForwardVector(FVector3f* Forward) const
{
    SubcubeToWorld({0.f, 1.f, 0.f}, Forward);
}

void URiggedHand::GrabStart(const FMatrix5& ToGrabbedCube)
{
    GrabStartInternal(ToGrabbedCube);
    Compass->GrabStart(ToGrabbedCube);
}

void URiggedHand::MoveHandStart(const FMatrix5& ToGrabbedCube)
{
    const FMatrix5& ToHand5 = GetMatrix();

    FVector3f E1H = ToHand5.GetColumn3(1).GetSafeNormal();
    FVector3f E2H = ToHand5.GetColumn3(0).Cross(E1H).GetSafeNormal();
    FVector3f E0H = E1H.Cross(E2H);

    FMatrix44f OriginToHand(E0H, E1H, E2H, ToHand5.GetColumn3(4));
    HandToOrigin = OriginToHand.Inverse();

    FXRMotionControllerData MotionControllerData;
    GetControllerData(&MotionControllerData);

    GetBoneTransform(MotionControllerData, EHandKeypoint::Wrist, WristTransform);
    UpdateMoveTransform();
}

void URiggedHand::RotateHandStart(const FMatrix5& ToGrabbedCube)
{
    RotateHandStartInternal();
    Compass->RotateHandStart();
}

void URiggedHand::RotateHand(const FMatrix5& SideRotation)
{
    RotateHandInternal(SideRotation);
    Compass->RotateHand(SideRotation);
}

void URiggedHand::ReleaseHand()
{
    ReleaseHandInternal();
    Compass->ReleaseHand();
}

double URiggedHand::GetBoundsRadius(float Matrix44) const
{
    return 100.f;
}

void URiggedHand::SetHandInternal(EControllerHand HandIn, FMatrix44f* CubeToHand)
{
    FHandInterface::SetHand(HandIn);

    ACubeGameMode* GameMode = FUtils::GetGameMode();
    if (!GameMode)
        return;

    SetSkinnedAssetAndUpdate(GameMode->GetSkeletalMesh(GetHand())->GetSkinnedAsset());

    InstancePreventGC = CreateObject4dInstance();


    float HandToCubePitch = -18.66f;
    float HandToCubeYaw = 25.2378f;
    float HandToCubeX = -1.74297f;

    float HandToCompassRY = 0.107332f;
    float HandToCompassRZ = -0.025363f;
    float HandToCompassPX = 0.249561f;

    if (GetHand() == EControllerHand::Left)
    {
        HandToCubePitch *= -1.f;
        HandToCubeYaw *= -1.f;
        HandToCubeX *= -1.f;

        HandToCompassRY *= -1.f;
        HandToCompassRZ *= -1.f;
        HandToCompassPX *= -1.f;
    }

    FRotator3f HandToCubeRotation = {HandToCubePitch, HandToCubeYaw, 22.3597f};
    HandToCube = FRotationTranslationMatrix44f(HandToCubeRotation, {HandToCubeX, -10.0073f, -5.64198f});

    *CubeToHand = FTransform3f(HandToCube).Inverse().ToMatrixNoScale();
    const FMatrix44f& H = *CubeToHand;
    SetCubeToThis({
        {H.M[0][0], H.M[1][0], H.M[2][0], 0.f, H.M[3][0]},
        {H.M[0][1], H.M[1][1], H.M[2][1], 0.f, H.M[3][1]},
        {H.M[0][2], H.M[1][2], H.M[2][2], 0.f, H.M[3][2]},
        {0.f,       0.f,       0.f,       1.f, 0.f},
        {0.f,       0.f,       0.f,       0.f, 1.f}
    });

    FQuat4f HandToCompassRotation = {-0.093301f, HandToCompassRY, HandToCompassRZ, 0.989511f};
    HandToCompass = FTransform3f(HandToCompassRotation, {HandToCompassPX, 1.57223f, 1.8078f}).ToMatrixNoScale();
}

void URiggedHand::GrabStartInternal(const FMatrix5& ToGrabbedCube)
{
    EnableObject4dInstance(ToGrabbedCube);
    CopyPoseFromSkeletalComponent(FUtils::GetGameMode()->GetSkeletalMesh(GetHand()));
}

void URiggedHand::RotateHandStartInternal()
{
    SaveTransform();
    UpdateGrabTransform();
}

void URiggedHand::RotateHandInternal(const FMatrix5& SideRotation)
{
    SetWorldTransform(FUtils::GetCubeTransform());
    Rotate(SideRotation);
    Update();
}

void URiggedHand::ReleaseHandInternal()
{
    DisableObject4dInstance();

    FXRMotionControllerData MotionControllerData;
    GetControllerData(&MotionControllerData);

    UpdateReleaseTransform(MotionControllerData);
}

bool URiggedHand::GetControllerData(FXRMotionControllerData* MotionControllerData)
{
    UHeadMountedDisplayFunctionLibrary::GetMotionControllerData(this, GetHand(), *MotionControllerData);

    bIsTracked = MotionControllerData->TrackingStatus != ETrackingStatus::NotTracked;
    return MotionControllerData->bValid && bIsTracked;
}

void URiggedHand::GetBoneTransform(const FXRMotionControllerData& MotionControllerData, EHandKeypoint HandKeypoint, FTransform& BoneTransform) const
{
    int32 Index = int32(HandKeypoint);
    FMatrix BoneMatrix = FixBoneRotation * FRotationMatrix::Make(MotionControllerData.HandKeyRotations[Index]);
    BoneMatrix.SetOrigin(MotionControllerData.HandKeyPositions[Index]);
    BoneTransform.SetFromMatrix(BoneMatrix);
}

void URiggedHand::UpdateMoveTransform()
{
    FTransform NewTransform;
    NewTransform.SetFromMatrix(FMatrix(HandToOrigin * FMatrix44f(WristTransform.ToMatrixNoScale())));
    FUtils::GetCubeActor()->SetActorTransform(NewTransform);
    UpdateGrabTransform();
}

void URiggedHand::UpdateGrabTransform()
{
    const FTransform& CubeTransform = FUtils::GetCubeTransform();
    SetWorldTransform(CubeTransform);
    Compass->SetWorldTransform(CubeTransform);
}

void URiggedHand::UpdateReleaseTransform(const FXRMotionControllerData& MotionControllerData)
{
    SetWorldTransform(WristTransform);
    Compass->SetRelativeTransform(FTransform3d(FTransform3f(HandToCompass)));

    static const FName BoneNames[26] = {
        "Palm",
        "Wrist",
        "Thumb_Joint_0",
        "Thumb_Joint_1",
        "Thumb_Joint_2",
        "Thumb_Joint_Tip",
        "Index_Joint_0",
        "Index_Joint_1",
        "Index_Joint_2",
        "Index_Joint_3",
        "Index_Joint_Tip",
        "Middle_Joint_0",
        "Middle_Joint_1",
        "Middle_Joint_2",
        "Middle_Joint_3",
        "Middle_Joint_Tip",
        "Ring_Joint_0",
        "Ring_Joint_1",
        "Ring_Joint_2",
        "Ring_Joint_3",
        "Ring_Joint_Tip",
        "Pinky_Joint_0",
        "Pinky_Joint_1",
        "Pinky_Joint_2",
        "Pinky_Joint_3",
        "Pinky_Joint_Tip"
    };

    auto SetBoneTransform = [this](EHandKeypoint HandKeypoint, const FTransform& BoneTransform)
    {
        SetBoneTransformByName(BoneNames[int8(HandKeypoint)], BoneTransform, EBoneSpaces::WorldSpace);
    };

    SetBoneTransform(EHandKeypoint::Wrist, WristTransform);

    auto UpdateBone = [this, &MotionControllerData, &SetBoneTransform](EHandKeypoint HandKeypoint)
    {
        FTransform Transform;
        GetBoneTransform(MotionControllerData, HandKeypoint, Transform);
        SetBoneTransform(HandKeypoint, Transform);
    };

    UpdateBone(EHandKeypoint::Palm);
    UpdateBone(EHandKeypoint::ThumbMetacarpal);
    UpdateBone(EHandKeypoint::ThumbProximal);
    UpdateBone(EHandKeypoint::ThumbDistal);
    UpdateBone(EHandKeypoint::ThumbTip);
    UpdateBone(EHandKeypoint::IndexMetacarpal);
    UpdateBone(EHandKeypoint::IndexProximal);
    UpdateBone(EHandKeypoint::IndexIntermediate);
    UpdateBone(EHandKeypoint::IndexDistal);
    UpdateBone(EHandKeypoint::IndexTip);
    UpdateBone(EHandKeypoint::MiddleMetacarpal);
    UpdateBone(EHandKeypoint::MiddleProximal);
    UpdateBone(EHandKeypoint::MiddleIntermediate);
    UpdateBone(EHandKeypoint::MiddleDistal);
    UpdateBone(EHandKeypoint::MiddleTip);
    UpdateBone(EHandKeypoint::RingMetacarpal);
    UpdateBone(EHandKeypoint::RingProximal);
    UpdateBone(EHandKeypoint::RingIntermediate);
    UpdateBone(EHandKeypoint::RingDistal);
    UpdateBone(EHandKeypoint::RingTip);
    UpdateBone(EHandKeypoint::LittleMetacarpal);
    UpdateBone(EHandKeypoint::LittleProximal);
    UpdateBone(EHandKeypoint::LittleIntermediate);
    UpdateBone(EHandKeypoint::LittleDistal);
    UpdateBone(EHandKeypoint::LittleTip);
}

void URiggedHand::SubcubeToWorld(const FVector3f& SubcubeVector, FVector3f* WorldVector) const
{
    *WorldVector = FVector3f(WristTransform.TransformVector(FVector(HandToCube.TransformVector(SubcubeVector))));
}
