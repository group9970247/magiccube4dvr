#include "CubeMotionControllerComponent.h"
//#include "CubeGameMode.h"

//UCubeMotionControllerComponent::UCubeMotionControllerComponent() :
//    FHandInterface(
//        Cast<UPrimitiveComponent>(this),
//        -0.2f,
//        ACubeGameMode::MarginLength / 2.f / ACubeGameMode::CubeSideLength)
//{
//    SetShowDeviceModel(true);
//}

void UCubeMotionControllerComponent::SetHand(EControllerHand HandIn)
{
    SetTrackingMotionSource(HandIn == EControllerHand::Left ? "Left" : "Right");
}

//void UCubeMotionControllerComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
//{
//    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

//    if (IsActive())
//         TickHand(DeltaTime);
//}

bool UCubeMotionControllerComponent::IsLeft() const
{
    return MotionSource == "Left";
}

void UCubeMotionControllerComponent::GetGrabPoint(FVector& GrabPoint) const
{
    GrabPoint = GetComponentLocation();
}

void UCubeMotionControllerComponent::GetDownVector(FVector3f* Down) const
{

}

void UCubeMotionControllerComponent::GetForwardVector(FVector3f* Forward) const
{
    *Forward = FVector3f(UMotionControllerComponent::GetForwardVector());
}

void UCubeMotionControllerComponent::GrabStart(const FMatrix5& ToGrabbedCube)
{

}

void UCubeMotionControllerComponent::MoveHandStart(const FMatrix5& ToGrabbedCube)
{
//    FVector GrabPoint;
//    GetGrabPoint(GrabPoint);

//    ACubeGameMode* CubeGameMode = ACubeGameMode::Get(this);
//    const UCubeComponent* GrabbedCube = CubeGameMode->GetGrid()->At(GrabbedCell);
//    GetCubeActor()->AddActorWorldOffset(GrabPoint - GrabbedCube->Bounds.Origin);

//    AttachCubeToHand();
}

void UCubeMotionControllerComponent::RotateHand(const FMatrix5& SideRotation)
{
//    ACubeGameMode* CubeGameMode = ACubeGameMode::Get(this);
//    FVector Origin = CubeGameMode->GetGrid()->At(GrabbedCell)->Bounds.Origin;

//    FQuat4f Rotation(FMatrix44f(E0, E1, E2, FVector3f::ZeroVector));
//    SetWorldLocationAndRotation(Origin, FQuat(Rotation));
}

double UCubeMotionControllerComponent::GetBoundsRadius(float Matrix44) const
{
    return 0.0;
}
