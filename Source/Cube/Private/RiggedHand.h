#pragma once

#include "HandInterface.h"

#include "Components/PoseableMeshComponent.h"
#include "RiggedHand.generated.h"


struct FXRMotionControllerData;
enum class EHandKeypoint : uint8;

UCLASS()
class URiggedHand : public UPoseableMeshComponent, public FHandInterface
{
    GENERATED_BODY()

public:
    URiggedHand();
    virtual void OnRegister() override;
    virtual void SetHand(EControllerHand HandIn) override;
    virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
    virtual FBoxSphereBounds CalcBounds(const FTransform& LocalToWorld) const override;

protected:
    virtual bool IsLeft() const override;
    virtual void GetGrabPoint(FVector& GrabPoint) const override;
    virtual void GetDownVector(FVector3f* Down) const override;
    virtual void GetForwardVector(FVector3f* Forward) const override;
    virtual void GrabStart(const FMatrix5& ToGrabbedCube) override;
    virtual void MoveHandStart(const FMatrix5& ToGrabbedCube) override;
    virtual void RotateHandStart(const FMatrix5& ToGrabbedCube) override;
    virtual void RotateHand(const FMatrix5& SideRotation) override;
    virtual void ReleaseHand() override;

    virtual double GetBoundsRadius(float Matrix44) const override;

private:
    FTransform WristTransform;

    FMatrix44f HandToCube;
    FMatrix44f HandToOrigin;
    FMatrix44f HandToCompass;
    FMatrix FixBoneRotation;

    UPROPERTY()
    UMaterialInstanceDynamic* InstancePreventGC;

    UPROPERTY()
    class UCompass* Compass;


    void SetHandInternal(EControllerHand HandIn, FMatrix44f* CubeToHand);
    void GrabStartInternal(const FMatrix5& ToGrabbedCube);
    void RotateHandStartInternal();
    void RotateHandInternal(const FMatrix5& SideRotation);
    void ReleaseHandInternal();

    bool GetControllerData(FXRMotionControllerData* MotionControllerData);
    void GetBoneTransform(const FXRMotionControllerData& MotionControllerData, EHandKeypoint HandKeypoint, FTransform& BoneTransform) const;
    void UpdateMoveTransform();
    void UpdateGrabTransform();
    void UpdateReleaseTransform(const FXRMotionControllerData& MotionControllerData);

    void SubcubeToWorld(const FVector3f& SubcubeVector, FVector3f* WorldVector) const;
};
