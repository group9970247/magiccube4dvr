#include "MenuWidget.h"
#include "CubePawn.h"
#include "Utils.h"
#include "CubeActor.h"
#include "RotateSide.h"
#include "CubeSaveGame.h"
#include "CubeSettings.h"
#include "MenuActor.h"
#include "Steam.h"

#include "Blueprint/WidgetLayoutLibrary.h"
#include "Components/Border.h"
#include "Components/CanvasPanelSlot.h"
#include "Kismet/GameplayStatics.h"
#include "Components/TextBlock.h"
#include "Components/WidgetSwitcher.h"


void UMenuWidget::InitializeIntensity()
{
    InitializeBP();
    SetIntensity(IntensityMenu);
}

void UMenuWidget::SelectDefault()
{
    Selected = {3, NewGameWasStarted() ? 0 : 1};
    Select();
}

bool UMenuWidget::NewGameWasStarted() const
{
    return !Continue;
}

void UMenuWidget::MakeStep(FIntVector2 Step)
{
    do
    {
        FIntVector2 ButtonGridSize = {Buttons.Num(), Buttons[0].Num()};
        Selected = {
            (Selected.X + Step.X + ButtonGridSize.X) % ButtonGridSize.X,
            (Selected.Y + Step.Y + ButtonGridSize.Y) % ButtonGridSize.Y
        };
    }
    while (!Buttons[Selected.X][Selected.Y]);

    FUtils::GetMenuActor()->Select();
    Select();
}

void UMenuWidget::Press()
{
    UCubeSettings* Settings = GetMutableDefault<UCubeSettings>();

    if (Selected.X == 0)
    {
        if (!UGameplayStatics::DeleteGameInSlot(SaveFileName(Selected.Y), 0))
            return;

        SaveSlots[Selected.Y]->SetText(FText::FromString("[empty]"));
    }
    else if (Selected.X == 1)
    {
        FString Time;
        auto SetTime = [&Time]()
        {
            Time = FDateTime::Now().ToString().RightChop(2);
        };

        SetTime();
        while (Time == LastTime)
        {
            FPlatformProcess::Sleep(0.1f);
            SetTime();
        }

        LastTime = Time;

        USaveGame* SaveGame = UGameplayStatics::CreateSaveGameObject(UCubeSaveGame::StaticClass());
        UCubeSaveGame* CubeSaveGame = Cast<UCubeSaveGame>(SaveGame);
        if (!CubeSaveGame)
            return;

        CubeSaveGame->Save();
        if (UGameplayStatics::SaveGameToSlot(CubeSaveGame, SaveFileName(Selected.Y), 0))
            SaveSlots[Selected.Y]->SetText(FText::FromString(Time));
    }
    else if (Selected.X == 2)
    {
        USaveGame* SaveGame = UGameplayStatics::LoadGameFromSlot(SaveFileName(Selected.Y), 0);
        UCubeSaveGame* CubeSaveGame = Cast<UCubeSaveGame>(SaveGame);
        if (!CubeSaveGame)
            return;

        bSolved = CubeSaveGame->GetSolved();
        CubeSaveGame->Load();
        LeaveMenu();
    }
    else if (Selected.X == 3)
    {
        if (Selected.Y == 0)
            FUtils::GetPawn()->MenuStarted();
        else if (Selected.Y == 1)
        {
            bSolved = false;
            StartNewGame();
        }
        else if (Selected.Y == 2)
        {
            bSolved = true;
            StartNewGame();
        }
        else if (Selected.Y == 4)
            SwitchOption(Settings->bSound, 0);
        else if (Selected.Y == 5)
            SwitchOption(Settings->bHaptics, 1);
        else if (Selected.Y == 6)
            SwitchOption(Settings->bCompass, 2);
        else if (Selected.Y == 7)
            FUtils::Quit();
    }

    FUtils::GetMenuActor()->Press();
}

void UMenuWidget::TickMenu(float DeltaSeconds)
{
    float IntensityStep = DeltaSeconds / 3.f;
    if (FUtils::InMenu())
    {
        if (Intensity > IntensityMenu)
            SetIntensity(FMath::Max(Intensity - IntensityStep, IntensityMenu));
        
        WidgetSwitcher->SetActiveWidgetIndex(FUtils::ValveIndexController() ? 0 : 1);
    }
    else
    {
        if (Intensity < IntensityGame)
            SetIntensity(FMath::Min(Intensity + IntensityStep, IntensityGame));
    }
}

const FString& UMenuWidget::GetLastTime() const
{
    return LastTime;
}

bool UMenuWidget::GetSolved() const
{
    return bSolved;
}

void UMenuWidget::SetWidgets(const TArray<UWidget*>& Column0, const TArray<UWidget*>& Column1, const TArray<UWidget*>& Column2, const TArray<UWidget*>& Column3, const TArray<UWidget*>& Column4, UBorder* SelectionIn, UWidgetSwitcher* WidgetSwitcherIn)
{
    for (int32 Index = 0; Index < 8; Index++)
    {
        UTextBlock* SaveSlot = Cast<UTextBlock>(Column0[Index]);
        SaveSlots.Add(SaveSlot);
        const USaveGame* SaveGame = UGameplayStatics::LoadGameFromSlot(SaveFileName(Index), 0);
        if (const UCubeSaveGame* CubeSaveGame = Cast<UCubeSaveGame>(SaveGame))
            SaveSlot->SetText(FText::FromString(CubeSaveGame->GetTime()));
    }


    Buttons.SetNum(4);

    for (int32 Index = 0; Index < SaveSlots.Num(); Index++)
        Buttons[0].Add(UWidgetLayoutLibrary::SlotAsCanvasSlot(Column1[Index]));

    for (int32 Index = 0; Index < SaveSlots.Num(); Index++)
        Buttons[1].Add(UWidgetLayoutLibrary::SlotAsCanvasSlot(Column2[Index]));

    for (int32 Index = 0; Index < SaveSlots.Num(); Index++)
        Buttons[2].Add(UWidgetLayoutLibrary::SlotAsCanvasSlot(Column3[Index]));


    Continue = Column4[0];
    Buttons[3].Add(nullptr);
    Continue->SetVisibility(ESlateVisibility::Hidden);

    for (int32 Index = 0; Index < 2; Index++)
        Buttons[3].Add(UWidgetLayoutLibrary::SlotAsCanvasSlot(Column4[1 + Index]));

    Buttons[3].Add(nullptr);

    for (int32 Index = 0; Index < 3; Index++)
    {
        Buttons[3].Add(UWidgetLayoutLibrary::SlotAsCanvasSlot(Column4[3 + 2 * Index]));
        SettingsValues.Add(Cast<UTextBlock>(Column4[3 + 2 * Index + 1]));
    }

    Buttons[3].Add(UWidgetLayoutLibrary::SlotAsCanvasSlot(Column4[9]));

    const UCubeSettings* Settings = GetMutableDefault<UCubeSettings>();

    UpdateValue(Settings->bSound, 0);
    UpdateValue(Settings->bHaptics, 1);
    UpdateValue(Settings->bCompass, 2);


    Selection = UWidgetLayoutLibrary::SlotAsCanvasSlot(SelectionIn);
    Select();

    WidgetSwitcher = WidgetSwitcherIn;
}

void UMenuWidget::Select()
{
    if (Buttons.Num() == 0)
        return;

    const UCanvasPanelSlot* Button = Buttons[Selected.X][Selected.Y];
    FVector2D Margin = {8.0, 8.0};

    Selection->SetPosition(Button->GetPosition() - Margin);
    Selection->SetSize(Button->GetSize() + Margin * 2.0);
}

void UMenuWidget::StartNewGame()
{
    ACubeActor* Actor = FUtils::GetCubeActor();
    Actor->ResetGrid();

    if (!bSolved)
    {
        FMath::SRandInit(FDateTime::UtcNow().GetMillisecond());

        for (int32 Iteration = 0; Iteration < 128; Iteration++)
        {
            int8 SideAxisIndex = FMath::Rand() % 4;

            TArray<int8> OtherAxises;
            FUtils::GetOtherAxises(SideAxisIndex, &OtherAxises);

            int8 RotationAxis0Index = FMath::Rand() % 3;
            int8 RotationAxis0 = OtherAxises[RotationAxis0Index];
            OtherAxises.RemoveAt(RotationAxis0Index);

            FRotateSide::Start(SideAxisIndex, bool(FMath::Rand() % 2), RotationAxis0, OtherAxises[FMath::Rand() % 2]);
            FRotateSide::End(1 + FMath::Rand() % 3);
        }
    }

    Actor->UpdateSubcubes();
    LeaveMenu();
}

FString UMenuWidget::SaveFileName(int32 Index) const
{
    return "CubeSave" + FString::FromInt(Index);
}

void UMenuWidget::LeaveMenu()
{
    if (!NewGameWasStarted())
    {
        Buttons[3][0] = UWidgetLayoutLibrary::SlotAsCanvasSlot(Continue);
        Continue->SetVisibility(ESlateVisibility::Visible);
        Continue = nullptr;
    }

    FUtils::GetPawn()->MenuStarted();
}

void UMenuWidget::UpdateValue(bool bValue, int8 Index)
{
    SettingsValues[Index]->SetText(FText::FromString(bValue ? TEXT("On") : TEXT("Off")));
}

void UMenuWidget::SwitchOption(bool& bOption, int8 Index)
{
    bOption = !bOption;
    GetMutableDefault<UCubeSettings>()->SaveConfig();
    UpdateValue(bOption, Index);
}

void UMenuWidget::SetIntensity(float IntensityIn)
{
    Intensity = IntensityIn;
    IntensityChanged(Intensity);
}
