#pragma once

#include "UObject/Object.h"
#include "CubeSettings.generated.h"

UCLASS(config = CubeSettings)
class UCubeSettings : public UObject
{
    GENERATED_BODY()

public:
    UPROPERTY(config)
    bool bSound = true;

    UPROPERTY(config)
    bool bHaptics = true;

    UPROPERTY(config)
    bool bCompass = true;
};
