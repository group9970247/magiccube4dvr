#include "Test.h"
#include "Camera.h"

#include "Engine/World.h"


ATest::ATest()
{
    RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
}

void ATest::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
    Super::PostEditChangeProperty(PropertyChangedEvent);

    if (!GetWorld() || !GetWorld()->IsGameWorld())
        return;

    const FProperty* MemberProperty = PropertyChangedEvent.MemberProperty;
    if (MemberProperty && MemberProperty->GetFName() == GET_MEMBER_NAME_CHECKED(ATest, Angles))
        FCamera::SetAngles(Angles);
}

void ATest::Test()
{

}
