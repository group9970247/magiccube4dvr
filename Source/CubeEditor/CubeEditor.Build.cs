using UnrealBuildTool;

public class CubeEditor : ModuleRules
{
    public CubeEditor(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        PrivateDependencyModuleNames.AddRange(new string[] {
            "Cube",

            "Core",
            "CoreUObject",
            "Engine"
        });
    }
}
