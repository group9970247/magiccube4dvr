#pragma once

#include "GameFramework/Actor.h"
#include "Test.generated.h"

UCLASS()
class ATest : public AActor
{
    GENERATED_BODY()

public:
    UPROPERTY(EditAnywhere)
    FVector3f Angles = {-0.4f, 2.2f, 0.7f};


    ATest();
    virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

    UFUNCTION(CallInEditor)
    void Test();
};
