using UnrealBuildTool;
using System.Collections.Generic;

public class CubeTarget : TargetRules
{
    public CubeTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Game;
        DefaultBuildSettings = BuildSettingsVersion.V2;
        IncludeOrderVersion = EngineIncludeOrderVersion.Latest;
        ExtraModuleNames.Add("Cube");
    }
}
